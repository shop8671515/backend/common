package pl.com.rszewczyk.common.feign.exception;

public interface IExceptionMetadata {

  String getMessage();

  String getErrorCode();

  int getHttpStatus();

  String getSolution();
}
