package pl.com.rszewczyk.common.feign;

import com.fasterxml.jackson.databind.ObjectMapper;
import feign.Feign;
import feign.form.ContentType;
import feign.form.FormEncoder;
import feign.form.MultipartFormContentProcessor;
import feign.jackson.JacksonEncoder;
import feign.okhttp.OkHttpClient;
import feign.slf4j.Slf4jLogger;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Objects;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import pl.com.rszewczyk.common.feign.exception.GenericException;
import pl.com.rszewczyk.common.feign.writers.MetadataSourceWriter;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class Api {

  public static Feign.Builder builder(@NotNull ObjectMapper objectMapper) {
    FormEncoder encoder = new FormEncoder(new JacksonEncoder(objectMapper));
    MultipartFormContentProcessor processor =
        (MultipartFormContentProcessor) encoder.getContentProcessor(ContentType.MULTIPART);
    processor.addFirstWriter(new MetadataSourceWriter());
    return Feign.builder()
        .client(new OkHttpClient())
        .encoder(encoder)
        .decoder(new ApiResponseFeignDecoder(objectMapper))
        .logger(new Slf4jLogger());
  }

  public static <T> T build(
      @NotBlank String uri, @NotNull Class<T> apiClass, @NotNull ObjectMapper objectMapper) {
    String url = uri;

    try {
      if (Objects.isNull(new URI(url).getScheme())) {
        url = "http://" + url;
      }
    } catch (URISyntaxException exception) {
      throw GenericException.serverError(
          String.format("Niepoprawny adres przy budowaniu klienta feign %s", uri),
          "INVALID_FEIGN_ADDRESS",
          exception);
    }
    return builder(objectMapper).target(apiClass, url);
  }
}
