package pl.com.rszewczyk.common.feign.writers;

import feign.codec.EncodeException;
import feign.form.multipart.AbstractWriter;
import feign.form.multipart.Output;
import java.io.IOException;
import java.io.InputStream;
import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import pl.com.rszewczyk.common.feign.metadata.IMetadataSource;

public class MetadataSourceWriter extends AbstractWriter {
  private static final int BUFFER_SIZE = 4096;

  @Override
  public boolean isApplicable(Object value) {
    return value instanceof IMetadataSource;
  }

  protected void write(@NotNull Output output, @Nullable String key, @NotNull Object value) {
    IMetadataSource dataMetadata = (IMetadataSource) value;
    this.writeFileMetadata(output, key, dataMetadata.getName(), dataMetadata.getContentType());

    try (InputStream inputStream = dataMetadata.getInputStream()) {
      byte[] buffer = new byte[BUFFER_SIZE];

      for (int length = inputStream.read(buffer); length > 0; length = inputStream.read(buffer)) {
        output.write(buffer, 0, length);
      }
    } catch (IOException exception) {
      String message = String.format("Writing DataMetadata '%s' content error", key);
      throw new EncodeException(message, exception);
    }
  }
}
