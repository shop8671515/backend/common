package pl.com.rszewczyk.common.feign;

import feign.RequestTemplate;
import feign.codec.EncodeException;
import feign.codec.Encoder;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

@RequiredArgsConstructor
public class PageableQueryEncoder implements Encoder {
  private final Encoder delegate;

  @Override
  public void encode(Object object, Type bodyType, RequestTemplate template)
      throws EncodeException {
    if (object instanceof Pageable) {
      Pageable pageable = (Pageable) object;
      template.query("page", pageable.getPageNumber() + "");
      template.query("size", pageable.getPageSize() + "");

      pageable.getSort();
      Collection<String> existingSorts = template.queries().get("sort");
      List<String> sortQueries =
          Optional.ofNullable(existingSorts)
              .map(ArrayList::new)
              .orElseGet(() -> new ArrayList<>(existingSorts));
      for (Sort.Order order : pageable.getSort()) {
        sortQueries.add(order.getProperty() + "," + order.getDirection());
      }
      template.query("sort", sortQueries);

    } else {
      delegate.encode(object, bodyType, template);
    }
  }
}
