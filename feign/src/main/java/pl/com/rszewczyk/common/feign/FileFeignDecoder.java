package pl.com.rszewczyk.common.feign;

import feign.Response;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

public class FileFeignDecoder implements ITypeDecoder {
  private static final String FILE_PREFIX = "feign-file-";
  private static final String FAILED_MESSAGE = "Failed to decode feign file!";

  @Override
  public Object decode(Response response, Type type) throws IOException {
    File file = File.createTempFile(FILE_PREFIX, "");

    try (InputStream inputStream = response.body().asInputStream()) {
      Files.copy(inputStream, file.toPath(), StandardCopyOption.REPLACE_EXISTING);
    } catch (IOException exception) {
      throw new RuntimeException(FAILED_MESSAGE);
    }

    return file;
  }

  @Override
  public boolean canProcess(Class<?> clazz) {
    return clazz.isAssignableFrom(File.class);
  }
}
