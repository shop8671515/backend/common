package pl.com.rszewczyk.common.feign.metadata;

import java.io.IOException;
import java.io.InputStream;
import javax.validation.constraints.NotNull;

public interface IMetadataSource {

  @NotNull
  InputStream getInputStream() throws IOException;

  @NotNull
  String getContentType();

  @NotNull
  String getName();
}
