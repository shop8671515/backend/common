package pl.com.rszewczyk.common.feign;

import java.util.List;
import java.util.Map;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ApiResponse<T> {
  private final int statusCode;
  private final Map<String, List<String>> headers;
  private final T data;

  public ApiResponse(int statusCode, @NotNull Map<String, List<String>> headers) {
    this(statusCode, headers, (T) null);
  }
}
