package pl.com.rszewczyk.common.feign.metadata;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import javax.annotation.Nullable;
import lombok.Getter;

public class ByteArrayMetadataSource implements IMetadataSource {
  @Getter private final String name;
  private final byte[] bytes;
  @Getter private final String contentType;

  public ByteArrayMetadataSource(
      @Nullable String name, byte[] bytes, @Nullable String contentType) {
    this.name = name;
    this.bytes = bytes;
    this.contentType = contentType;
  }

  @Override
  public InputStream getInputStream() {
    return new ByteArrayInputStream(this.bytes);
  }

  @Override
  public String toString() {
    return "ByteArrayMetadataSource{name='"
        + this.name
        + "', contentType='"
        + this.contentType
        + "'}";
  }
}
