package pl.com.rszewczyk.common.feign;

import com.fasterxml.jackson.databind.ObjectMapper;
import feign.Request;
import feign.RequestTemplate;
import feign.codec.EncodeException;
import feign.codec.Encoder;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;

@RequiredArgsConstructor
@SuppressWarnings("unchecked")
public class FeignQueryParamsAggregationEncoder implements Encoder {
  private final ObjectMapper mapper;
  private final Encoder delegate;

  @Override
  public void encode(Object body, Type type, RequestTemplate requestTemplate)
      throws EncodeException {
    if (Objects.isNull(body)
        || !Request.HttpMethod.GET.name().equalsIgnoreCase(requestTemplate.method())) {
      delegate.encode(body, type, requestTemplate);
      return;
    }
    Map<String, Object> queryParams = mapper.convertValue(body, Map.class);
    queryParams.forEach((key, value) -> queryTemplate(key, value, requestTemplate));
  }

  private void queryTemplate(String key, Object value, RequestTemplate requestTemplate) {
    if (value instanceof Collection) {
      if (CollectionUtils.isNotEmpty((Collection<Object>) value)) {
        List<String> values =
            ((Collection<Object>) value)
                .stream()
                    .map(o -> mapper.convertValue(o, String.class))
                    .collect(Collectors.toList());
        requestTemplate.query(key, values);
      }
    } else if (Objects.nonNull(value)) {
      requestTemplate.query(key, mapper.convertValue(value, String.class));
    }
  }
}
