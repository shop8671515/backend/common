package pl.com.rszewczyk.common.feign;

import com.fasterxml.jackson.databind.ObjectMapper;
import feign.FeignException;
import feign.Response;
import feign.Types;
import feign.codec.Decoder;
import feign.jackson.JacksonDecoder;
import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.validation.constraints.NotNull;

public class ApiResponseFeignDecoder implements Decoder {
  private final JacksonDecoder jacksonDecoder;
  private final List<ITypeDecoder> decoders;

  public ApiResponseFeignDecoder(@NotNull ObjectMapper objectMapper) {
    this.jacksonDecoder = new JacksonDecoder(objectMapper);
    this.decoders = List.of(new InputStreamFeignDecoder(), new FileFeignDecoder());
  }

  @Override
  public Object decode(Response response, Type type) throws IOException, FeignException {
    if (type instanceof ParameterizedType
        && Types.getRawType(type).isAssignableFrom(ApiResponse.class)) {
      Type responsedBodyType = ((ParameterizedType) type).getActualTypeArguments()[0];
      Object body = this.decodeByDelegate(response, responsedBodyType);
      Map<String, List<String>> responseHeaders =
          response.headers().entrySet().stream()
              .collect(
                  Collectors.toUnmodifiableMap(
                      Map.Entry::getKey, (e) -> List.copyOf(e.getValue())));
      return new ApiResponse<>(response.status(), responseHeaders, body);
    } else {
      return this.decodeByDelegate(response, type);
    }
  }

  private Object decodeByDelegate(@NotNull Response response, @NotNull Type type)
      throws IOException {
    Class<?> rawType = Types.getRawType(type);
    Iterator<ITypeDecoder> iterator = this.decoders.iterator();

    ITypeDecoder decoder;
    do {
      if (!iterator.hasNext()) {
        return this.jacksonDecoder.decode(response, type);
      }
      decoder = iterator.next();
    } while (!decoder.canProcess(rawType));

    return decoder.decode(response, type);
  }
}
