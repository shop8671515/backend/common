package pl.com.rszewczyk.common.feign.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({FeignConfiguration.class})
public class FeignAutoConfig {}
