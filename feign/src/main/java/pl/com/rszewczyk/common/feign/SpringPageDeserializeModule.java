package pl.com.rszewczyk.common.feign;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.springframework.data.domain.Page;
import pl.com.rszewczyk.common.api.support.paging.DeserializablePage;

public class SpringPageDeserializeModule extends SimpleModule {

  @Override
  public void setupModule(SetupContext context) {
    context.setMixInAnnotations(Page.class, PageMixIn.class);
    context.setMixInAnnotations(DeserializablePage.class, DeserializablePageMixIn.class);
  }

  @JsonDeserialize(as = PageWrapper.class)
  private interface PageMixIn {}

  @JsonDeserialize(as = DeserializablePage.class)
  private interface DeserializablePageMixIn {}
}
