package pl.com.rszewczyk.common.feign;

import feign.FeignException;
import feign.Response;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;

public class InputStreamFeignDecoder implements ITypeDecoder {
  private static final String INPUT_STREAM_FAILED_DECODE = "Failed to decode feign input stream!";

  @Override
  public Object decode(Response response, Type type) throws IOException, FeignException {
    InputStream inputStream = response.body().asInputStream();

    ByteArrayInputStream byteArrayInputStream;
    try {
      byteArrayInputStream = new ByteArrayInputStream(inputStream.readAllBytes());
    } catch (Throwable throwable) {
      throw new RuntimeException(INPUT_STREAM_FAILED_DECODE);
    } finally {
      inputStream.close();
    }
    return byteArrayInputStream;
  }

  @Override
  public boolean canProcess(Class<?> clazz) {
    return clazz.isAssignableFrom(InputStream.class);
  }
}
