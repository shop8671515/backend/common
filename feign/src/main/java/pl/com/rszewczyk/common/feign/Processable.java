package pl.com.rszewczyk.common.feign;

import javax.validation.constraints.NotNull;

public interface Processable {
  boolean canProcess(@NotNull Class<?> clazz);
}
