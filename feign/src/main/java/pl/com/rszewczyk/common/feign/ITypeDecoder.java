package pl.com.rszewczyk.common.feign;

import feign.codec.Decoder;

public interface ITypeDecoder extends Decoder, Processable {}
