package pl.com.rszewczyk.common.feign.metadata;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class FileMetadataSource implements IMetadataSource {
  private final File file;
  @Getter private final String contentType;

  @Override
  public InputStream getInputStream() throws IOException {
    return Files.newInputStream(this.file.toPath());
  }

  @Override
  public String getName() {
    return this.file.getName();
  }

  @Override
  public String toString() {
    return "FileMetadataSource{file=" + this.file + ", contentType='" + this.contentType + "'}";
  }
}
