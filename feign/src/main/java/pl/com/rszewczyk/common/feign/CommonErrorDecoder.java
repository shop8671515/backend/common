package pl.com.rszewczyk.common.feign;

import com.fasterxml.jackson.databind.ObjectMapper;
import feign.FeignException;
import feign.Response;
import feign.codec.ErrorDecoder;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import pl.com.rszewczyk.common.exception.InternalCommunicationException;
import pl.com.rszewczyk.common.exception.ObjectNotFoundException;
import pl.com.rszewczyk.common.exception.Violation;
import pl.com.rszewczyk.common.exception.ViolationException;
import pl.com.rszewczyk.http.problem.Error;
import pl.com.rszewczyk.http.problem.Problem;

public class CommonErrorDecoder implements ErrorDecoder {
  private final ObjectMapper objectMapper;
  private final BiFunction<String, String, Optional<Exception>> domainExceptionCodeMapping;
  private static final int NOT_FOUND = 404;
  private static final String NOT_FOUND_MESSAGE = "Object not found";

  public CommonErrorDecoder(ObjectMapper objectMapper) {
    this(objectMapper, null);
  }

  public CommonErrorDecoder(
      ObjectMapper objectMapper,
      BiFunction<String, String, Optional<Exception>> domainExceptionCodeMapping) {
    this.objectMapper = objectMapper;
    this.domainExceptionCodeMapping = domainExceptionCodeMapping;
  }

  @Override
  public Exception decode(String s, Response response) {
    Problem problem = getProblem(response, objectMapper);
    if (Objects.nonNull(problem)) {
      if (Objects.nonNull(problem.getErrors())
          && problem.getErrors().size() == 1
          && Objects.nonNull(domainExceptionCodeMapping)) {
        Optional<Exception> exception =
            domainExceptionCodeMapping.apply(
                problem.getErrors().get(0).getCode(), problem.getDetail());
        if (exception.isPresent()) {
          return exception.get();
        }
      }

      if (response.status() == 404) {
        return new ObjectNotFoundException(
            problem.getDetail(),
            Optional.ofNullable(problem.getErrors())
                .flatMap(errors -> errors.stream().findFirst().map(Error::getCode))
                .orElse(null));
      }

      Set<String> sourceErrorCodes =
          Optional.ofNullable(problem.getErrors())
              .map(errors -> errors.stream().map(Error::getCode).collect(Collectors.toSet()))
              .orElse(Collections.emptySet());
      List<Violation> sourceViolations =
          Optional.ofNullable(problem.getViolations())
              .map(
                  violations ->
                      violations.stream()
                          .map(
                              violation ->
                                  new Violation(violation.getField(), violation.getMessage()))
                          .collect(Collectors.toList()))
              .orElse(Collections.emptyList());
      if (!io.jsonwebtoken.lang.Collections.isEmpty(sourceViolations)) {
        return new ViolationException(
            problem.getDetail(), sourceErrorCodes, problem.getStatus().value(), sourceViolations);
      } else {
        return new InternalCommunicationException(
            problem.getDetail(), sourceErrorCodes, problem.getStatus().value(), sourceViolations);
      }
    }

    if (response.status() == NOT_FOUND) {
      return new ObjectNotFoundException(NOT_FOUND_MESSAGE);
    }

    return FeignException.errorStatus(s, response);
  }

  private Problem getProblem(Response response, ObjectMapper objectMapper) {
    if (Objects.isNull(response.body())) {
      return null;
    }

    try {
      Reader reader = response.body().asReader(StandardCharsets.UTF_8);
      if (!reader.markSupported()) {
        reader = new BufferedReader(reader, 1);
      }

      reader.mark(1);

      if (reader.read() == -1) {
        return null;
      }

      reader.reset();
      return objectMapper.readValue(reader, Problem.class);
    } catch (IOException ex) {
      return null;
    }
  }
}
