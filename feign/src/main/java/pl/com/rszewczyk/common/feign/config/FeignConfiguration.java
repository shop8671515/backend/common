package pl.com.rszewczyk.common.feign.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import feign.Logger;
import feign.codec.Encoder;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.FeignFormatterRegistrar;
import org.springframework.cloud.openfeign.support.SpringEncoder;
import org.springframework.context.annotation.Bean;
import org.springframework.format.datetime.standard.DateTimeFormatterRegistrar;
import org.springframework.stereotype.Component;
import pl.com.rszewczyk.common.feign.FeignQueryParamsAggregationEncoder;
import pl.com.rszewczyk.common.feign.PageableQueryEncoder;

@Component
@RequiredArgsConstructor
public class FeignConfiguration {
  private final ObjectFactory<HttpMessageConverters> messageConverters;
  private final ObjectMapper objectMapper;

  @Bean
  Logger.Level feignLoggerLever(@Value("${rszewczyk.feign.logging.level:BASIC}") String level) {
    return Logger.Level.valueOf(level);
  }

  @Bean
  public Encoder feignEncoder() {
    return new PageableQueryEncoder(
        new FeignQueryParamsAggregationEncoder(objectMapper, new SpringEncoder(messageConverters)));
  }

  @Bean
  public FeignFormatterRegistrar localDateFeignFormatterRegistrar() {
    return formatterRegistry -> {
      DateTimeFormatterRegistrar registrar = new DateTimeFormatterRegistrar();
      registrar.setUseIsoFormat(true);
      registrar.registerFormatters(formatterRegistry);
    };
  }
}
