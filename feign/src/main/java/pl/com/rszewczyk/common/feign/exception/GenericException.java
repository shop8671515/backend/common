package pl.com.rszewczyk.common.feign.exception;

import java.util.Collections;
import java.util.Map;
import javax.annotation.Nullable;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Getter;

@Getter
public class GenericException extends RuntimeException implements IExceptionMetadata {
  private final int httpStatus;
  private final String errorCode;
  private final String message;
  private final String solution;
  private final Map<String, Object> payload;

  private GenericException(
      @NotBlank String message,
      int httpStatus,
      @NotBlank String errorCode,
      @NotBlank String solution,
      @NotNull Map<String, Object> payload,
      @Nullable Throwable throwable) {
    super(throwable);
    this.message = message;
    this.httpStatus = httpStatus;
    this.errorCode = errorCode;
    this.solution = solution;
    this.payload = payload;
  }

  public static GenericException serverError(
      @NotBlank String message, @NotBlank String code, @Nullable Throwable exception) {
    return new GenericException(
        message,
        500,
        code,
        "Jeżeli problem nie ustępuje, należy zwrócić się do pomocy technicznej ze szczegółami problemu.",
        Collections.emptyMap(),
        exception);
  }
}
