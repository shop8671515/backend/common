package pl.com.rszewczyk.common.security.api;

import com.google.common.base.MoreObjects;
import com.google.common.base.Splitter;
import lombok.Builder;
import lombok.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import pl.com.rszewczyk.common.security.api.structure.Identity;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Value
@Builder
public class AccountIdentity implements Identity<UUID> {
    public static final AccountIdentity SYSTEM = fromHeaderString("00000000-0000-0000-0000-000000000000");
    public static final String SYSTEM_NAME = "System user";
    private static final String SEPARATOR = "|";
    private static final String DEFAULT_ROLE = "CLIENT";
    private static final GrantedAuthority DEFAULT_AUTHORITY = new SimpleGrantedAuthority(DEFAULT_ROLE);
    private final UUID id;
    private final GrantedAuthority authority;

    @Nullable
    public static AccountIdentity fromHeaderString(@Nullable String idWithRole) {
        if (Objects.isNull(idWithRole)) {
            return null;
        }

        List<String> parts = Splitter.on(SEPARATOR).omitEmptyStrings().limit(2).splitToList(idWithRole);

        if (parts.isEmpty()) {
            return null;
        }

        String id = parts.get(0);
        String role = DEFAULT_ROLE;

        if (parts.size() == 2) {
            role = parts.get(1);
        }

        return new AccountIdentity(UUID.fromString(id), new SimpleGrantedAuthority(role));
    }

    public GrantedAuthority getAuthority() {
        return MoreObjects.firstNonNull(authority, DEFAULT_AUTHORITY);
    }

    @Override
    public String getRole() {
        return getAuthority().getAuthority();
    }

    public String toHeaderString() {
        return id + SEPARATOR + getAuthority().getAuthority();
    }
}
