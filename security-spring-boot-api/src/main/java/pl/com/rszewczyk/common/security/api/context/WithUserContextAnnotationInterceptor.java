package pl.com.rszewczyk.common.security.api.context;

import lombok.RequiredArgsConstructor;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import pl.com.rszewczyk.common.security.api.AccountIdentity;
import pl.com.rszewczyk.common.security.api.UserContext;

import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.Optional;

import pl.com.rszewczyk.common.security.api.AccountIdentityService;

@RequiredArgsConstructor
public class WithUserContextAnnotationInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (!(handler instanceof HandlerMethod)) {
            return true;
        }

        WithUserContext withUserContext = getAnnotation((HandlerMethod) handler);
        AccountIdentity identity = UserContext.getIdentity();

        if (Objects.nonNull(withUserContext) && withUserContext.required() && Objects.isNull(identity)) {
            throw new ServletRequestBindingException("Missing request header '" + AccountIdentityService.HEADER_IDENTITY_NAME + "'");
        }

        return true;
    }

    @Nullable
    private static WithUserContext getAnnotation(@NotNull HandlerMethod handlerMethod) {
        WithUserContext withUserContext = handlerMethod.getMethodAnnotation(WithUserContext.class);
        return Optional.ofNullable(withUserContext)
                .orElseGet(() -> AnnotationUtils.findAnnotation(handlerMethod.getBeanType(), WithUserContext.class));
    }
}
