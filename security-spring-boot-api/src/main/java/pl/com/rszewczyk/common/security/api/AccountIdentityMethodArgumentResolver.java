package pl.com.rszewczyk.common.security.api;

import org.springframework.core.MethodParameter;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.annotation.AbstractNamedValueMethodArgumentResolver;

public class AccountIdentityMethodArgumentResolver extends AbstractNamedValueMethodArgumentResolver {

    public final static String HEADER_IDENTITY_NAME = AccountIdentityService.HEADER_IDENTITY_NAME;

    private final AccountIdentityService accountIdentityService;

    public AccountIdentityMethodArgumentResolver(AccountIdentityService accountIdentityService) {
        this.accountIdentityService = accountIdentityService;
    }

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.hasParameterAnnotation(AccountPrincipal.class);
    }

    @Override
    protected NamedValueInfo createNamedValueInfo(MethodParameter parameter) {
        AccountPrincipal annotation = parameter.getParameterAnnotation(AccountPrincipal.class);
        return new AccountIdentityMethodArgumentResolver.RequestHeaderNamedValueInfo(annotation);
    }

    @Override
    protected Object resolveName(String name, MethodParameter parameter, NativeWebRequest request) throws Exception {
        return accountIdentityService.resolve(request);
    }

    @Override
    protected void handleMissingValue(String name, MethodParameter parameter) throws ServletRequestBindingException {
        throw new ServletRequestBindingException("Missing request header '" + name + "' for method parameter of type " + parameter.getNestedParameterType().getSimpleName());
    }

    private static class RequestHeaderNamedValueInfo extends NamedValueInfo {

        private RequestHeaderNamedValueInfo(AccountPrincipal annotation) {
            super(HEADER_IDENTITY_NAME, annotation.required(), null);
        }
    }
}
