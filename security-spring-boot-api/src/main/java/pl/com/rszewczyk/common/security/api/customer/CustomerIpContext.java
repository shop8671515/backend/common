package pl.com.rszewczyk.common.security.api.customer;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.stereotype.Service;

import javax.annotation.Nullable;
import java.util.Objects;

@Slf4j
@Service
public class CustomerIpContext {
    private static final String MDC_KEY = "CUSTOMER_IP";

    public static void clean() {
        log.trace("Removing customer IP");
        MDC.remove(MDC_KEY);
    }

    public static void setCustomerIp(@Nullable String customerIp) {
        log.trace("Setting customer IP: {}", customerIp);

        if (Objects.nonNull(customerIp)) {
            MDC.put(MDC_KEY, customerIp);
        } else {
            CustomerIpContext.clean();
        }
    }
}
