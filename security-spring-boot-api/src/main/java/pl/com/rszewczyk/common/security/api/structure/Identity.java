package pl.com.rszewczyk.common.security.api.structure;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public interface Identity<T> {

    @NotNull
    T getId();

    @NotBlank
    String getRole();
}
