package pl.com.rszewczyk.common.security.api;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.annotation.Nullable;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@Service
public class UserContext {
    public static final String FALLBACK_ID_PROPERTY_NAME = "bit.context.user.fallbackId";
    private static AccountIdentity fallbackUserId;

    public UserContext(@Value("${" + FALLBACK_ID_PROPERTY_NAME + ":#{null}}")UUID fallbackUserId) {
        if (Objects.isNull(fallbackUserId)) {
            log.warn("No fallback user id set, ({}) -> ", FALLBACK_ID_PROPERTY_NAME);
        } else {
            UserContext.fallbackUserId = AccountIdentity.fromHeaderString(fallbackUserId.toString());
        }
    }

    public static void clean() {
        log.trace("Removing user identity {}", SecurityContextHolder.getContext().getAuthentication());
        SecurityContextHolder.clearContext();
        MDC.remove("USER_ID");
    }

    public static void setIdentity(@Nullable AccountIdentity accountIdentity) {
        log.trace("Setting user identity: {}", accountIdentity);

        if (Objects.nonNull(accountIdentity)) {
            SecurityContextHolder.getContext().setAuthentication(new AccountAuthentication(accountIdentity));
            MDC.put("USER_ID", accountIdentity.toHeaderString());
        } else {
            UserContext.clean();
            MDC.remove("USER_ID");
        }
    }

    @Nullable
    public static AccountIdentity getIdentity() {
        return Optional.ofNullable(SecurityContextHolder.getContext().getAuthentication())
                .map(Authentication::getPrincipal)
                .filter(AccountIdentity.class::isInstance)
                .map(AccountIdentity.class::cast)
                .orElse(null);
    }

    public static AccountIdentity getIdentityOrThrow() {
        return Objects.requireNonNull(getIdentity(), "UserContext->AccountIdentity");
    }

    public static UUID getUserId() {
        return Optional.ofNullable(getAccountIdentity())
                .map(AccountIdentity::getId)
                .orElse(null);
    }

    public static AccountIdentity getAccountIdentity() {
        return Optional.ofNullable(SecurityContextHolder.getContext().getAuthentication())
                .map(Authentication::getPrincipal)
                .filter(AccountIdentity.class::isInstance)
                .map(AccountIdentity.class::cast)
                .orElse(fallbackUserId);
    }
}
