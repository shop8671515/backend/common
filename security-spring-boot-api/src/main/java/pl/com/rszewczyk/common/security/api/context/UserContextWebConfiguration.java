package pl.com.rszewczyk.common.security.api.context;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import pl.com.rszewczyk.common.security.api.AccountIdentityService;

@Configuration
@RequiredArgsConstructor
public class UserContextWebConfiguration implements WebMvcConfigurer {
    private final AccountIdentityService accountIdentityService;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new SetUserContextInterceptor(accountIdentityService)).order(1);
        registry.addInterceptor(new WithUserContextAnnotationInterceptor()).order(2);
    }
}
