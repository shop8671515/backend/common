package pl.com.rszewczyk.common.security.api.context;

import lombok.RequiredArgsConstructor;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import pl.com.rszewczyk.common.security.api.AccountIdentityService;
import pl.com.rszewczyk.common.security.api.UserContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RequiredArgsConstructor
public class SetUserContextInterceptor extends HandlerInterceptorAdapter {
    private final AccountIdentityService accountIdentityService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        UserContext.setIdentity(accountIdentityService.resolveOnly(request));
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        UserContext.clean();
    }
}
