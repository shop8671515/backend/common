package pl.com.rszewczyk.common.security.api.customer;

import org.springframework.stereotype.Service;
import org.springframework.web.context.request.NativeWebRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Objects;

@Service
public class CustomerIpService {
    private static final String X_CUSTOMER_IP_HEADER = "X-Customer-Ip";

    public String resolve(NativeWebRequest request) {
        String[] headerValues = request.getHeaderValues(X_CUSTOMER_IP_HEADER);
        if (Objects.nonNull(headerValues)) {
            return String.join(",", Arrays.asList(headerValues));
        }
        return null;
    }

    public String resolve(HttpServletRequest request) {
        return request.getHeader(X_CUSTOMER_IP_HEADER);
    }
}
