package pl.com.rszewczyk.common.security.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

@Configuration
@Import({AccountIdentityService.class, UserContext.class})
public class SecurityApiAutoConfiguration {

    @Configuration
    public static class ArgumentResolverWebMvcConfig implements WebMvcConfigurer {
        @Autowired
        private AccountIdentityService accountIdentityService;

        @Override
        public void addArgumentResolvers(List<HandlerMethodArgumentResolver> resolvers) {
            resolvers.add(new AccountIdentityMethodArgumentResolver(accountIdentityService));
            resolvers.add(new ApplicationTypeMethodArgumentResolver());
        }
    }

    @Configuration
    @EnableGlobalMethodSecurity(prePostEnabled = true)
    @ConditionalOnProperty(value = "bit.security.enabled", havingValue = "true")
    public static class GlobalMethodSecurityConfig {}
}
