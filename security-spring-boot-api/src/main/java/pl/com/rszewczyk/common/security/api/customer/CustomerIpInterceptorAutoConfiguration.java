package pl.com.rszewczyk.common.security.api.customer;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(CustomerIpContextWebConfiguration.class)
public class CustomerIpInterceptorAutoConfiguration {
}
