package pl.com.rszewczyk.common.security.api;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.Collections;
import java.util.Objects;

@ToString
@EqualsAndHashCode
public class AccountAuthentication implements Authentication {
    private final AccountIdentity accountIdentity;

    public AccountAuthentication(AccountIdentity accountIdentity) {
        this.accountIdentity = Objects.requireNonNull(accountIdentity, "AccountIdentity");
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singleton(accountIdentity.getAuthority());
    }

    @Override
    public Object getCredentials() {
        return "";
    }

    @Override
    public Object getDetails() {
        return null;
    }

    @Override
    public Object getPrincipal() {
        return accountIdentity;
    }

    @Override
    public boolean isAuthenticated() {
        return true;
    }

    @Override
    public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {

    }

    @Override
    public String getName() {
        return accountIdentity.getId().toString();
    }
}
