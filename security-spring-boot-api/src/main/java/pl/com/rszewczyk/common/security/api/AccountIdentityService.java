package pl.com.rszewczyk.common.security.api;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.NativeWebRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

@Service
public class AccountIdentityService {
    public static final String HEADER_IDENTITY_NAME = "x-account-id";

    public AccountIdentity resolve(NativeWebRequest request) {
        String[] headerValues = request.getHeaderValues(HEADER_IDENTITY_NAME);
        if (Objects.nonNull(headerValues) && headerValues.length > 0) {
            return resolveInternal(headerValues[0]);
        }
        return null;
    }

    public AccountIdentity resolve(HttpServletRequest request) {
        String headerValue = request.getHeader(HEADER_IDENTITY_NAME);
        if (!StringUtils.isEmpty(headerValue)) {
            return resolveInternal(headerValue);
        }
        return null;
    }

    public AccountIdentity resolveOnly(HttpServletRequest request) {
        String headerValue = request.getHeader(HEADER_IDENTITY_NAME);
        if (!StringUtils.isEmpty(headerValue)) {
            return resolveInternal(headerValue, false);
        }
        return null;
    }

    private AccountIdentity resolveInternal(String headerValue) {
        return resolveInternal(headerValue, true);
    }

    private AccountIdentity resolveInternal(String headerValue, boolean setupContext) {
        AccountIdentity accountIdentity = AccountIdentity.fromHeaderString(headerValue);
        if (setupContext) {
            UserContext.setIdentity(accountIdentity);
        }
        return accountIdentity;
    }
}
