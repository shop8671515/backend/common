package pl.com.rszewczyk.common.helper.utils

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Component
import org.springframework.web.util.UriComponentsBuilder

@Component
class UriHelper {
    final ObjectMapper mapper

    UriHelper(ObjectMapper mapper) {
        this.mapper = mapper
        this.mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL)
    }

    UriComponentsBuilder buildQuery(String path, Object request, Pageable pageable = null) {
        Map<String, Object> params = validateAndSerialize(request)

        if (Objects.nonNull(pageable)) {
            params.put("page", pageable.getPageNumber())
            params.put("size", pageable.getPageSize())
            Optional.ofNullable(pageable.getSort())
                    .ifPresent({ params.put("sort", getSortQueries(it)) })
        }

        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromPath(path)
        params.entrySet().collect {
            it -> uriBuilder.queryParam(it.key, it.value instanceof Collection ? ((Collection<Object>) it.value).toArray() : it.value)
        }

        return uriBuilder
    }

    private Map<String, Object> validateAndSerialize(Object request) {
        Objects.nonNull(request) ?
                mapper.convertValue(request, Map.class) :
                [:]
    }

    private static Collection<String> getSortQueries(Sort sort) {
        sort.collect { "${it.getProperty()},${it.getDirection()}" } as Collection<String>
    }
}
