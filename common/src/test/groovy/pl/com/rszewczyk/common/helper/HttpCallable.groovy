package pl.com.rszewczyk.common.helper

import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.ResponseEntity

trait HttpCallable {
    abstract <T> ResponseEntity<T> httpGet(String url, ParameterizedTypeReference<T> responseType)

    abstract <T> ResponseEntity<T> httpGet(String url, Class<T> responseType, Object... urlVariables)

    abstract <T> ResponseEntity<T> httpGet(String url, Object queryObject, ParameterizedTypeReference<T> responseType, Object... urlVariables)

    abstract <T> ResponseEntity<T> httpGet(String url, Object queryObject, Class<T> responseType, Object... urlVariables)

    abstract <T> ResponseEntity<T> httpPost(String url, Object command, Class<T> responseType, Object... urlVariables)

    abstract <T> ResponseEntity<T> httpPut(String url, Object command, Class<T> responseType, Object... urlVariables)

    abstract <T> ResponseEntity<T> httpPut(String url, Object queryObject, Object command, Class<T> responseType, Object... urlVariables)

    abstract <T> ResponseEntity<T> httpPut(String url, Object command, ParameterizedTypeReference<T> responseType, Object... urlVariables)

    abstract <T> ResponseEntity<T> httpPut(String url, Object queryObject, Object command, ParameterizedTypeReference<T> responseType, Object... urlVariables)

    abstract <T> ResponseEntity<T> httpPatch(String url, Object command, Class<T> responseType, Object... urlVariables)

    abstract <T> ResponseEntity<T> httpPatch(String url, Object queryObject, Object command, Class<T> responseType, Object... urlVariables)

    abstract <T> ResponseEntity<T> httpPatch(String url, Object command, ParameterizedTypeReference<T> responseType, Object... urlVariables)

    abstract <T> ResponseEntity<T> httpPatch(String url, Object queryObject, Object command, ParameterizedTypeReference<T> responseType, Object... urlVariables)

    abstract <T> ResponseEntity<T> httpDelete(String url, Class<T> responseType, Object... urlVariables)

    abstract <T> ResponseEntity<T> httpDelete(String url, Object queryObject, Class<T> responseType, Object... urlVariables)
}