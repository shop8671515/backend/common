package pl.com.rszewczyk.common.helper

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.test.context.ActiveProfiles
import pl.com.rszewczyk.common.security.api.AccountIdentity
import pl.com.rszewczyk.common.helper.utils.UriHelper
import spock.lang.Specification

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT

@SpringBootTest(webEnvironment = RANDOM_PORT, classes = [UriHelper])
@ActiveProfiles("test")
class RestSpecIT extends Specification implements HttpCallable {
    protected static UUID ACCOUNT_ID = UUID.randomUUID()

    @Autowired
    TestRestTemplate restTemplate

    @Autowired
    UriHelper uriHelper

    public <T> ResponseEntity<T> httpGet(String url, ParameterizedTypeReference<T> responseType) {
        restTemplate.exchange(url, HttpMethod.GET, assembleHeadersWithBody(), responseType)
    }

    public <T> ResponseEntity<T> httpGet(String url, Class<T> responseType, Object... urlVariables) {
        restTemplate.exchange(url, HttpMethod.GET, assembleHeadersWithBody(), responseType, urlVariables)
    }

    public <T> ResponseEntity<T> httpGet(String url, Object queryObject, ParameterizedTypeReference<T> responseType, Object... urlVariables) {
        String query = uriHelper.buildQuery(url, queryObject).build().toUriString()
        restTemplate.exchange(query, HttpMethod.GET, assembleHeadersWithBody(), responseType, urlVariables)
    }

    public <T> ResponseEntity<T> httpGet(String url, Object queryObject, Class<T> responseType, Object... urlVariables) {
        String query = uriHelper.buildQuery(url, queryObject).build().toUriString()
        restTemplate.exchange(query, HttpMethod.GET, assembleHeadersWithBody(), responseType, urlVariables)
    }

    public <T> ResponseEntity<T> httpPost(String url, Object command, Class<T> responseType, Object... urlVariables) {
        restTemplate.exchange(url, HttpMethod.POST, assembleHeadersWithBody(command), responseType, urlVariables)
    }

    public <T> ResponseEntity<T> httpPut(String url, Object command, Class<T> responseType, Object... urlVariables) {
        restTemplate.exchange(url, HttpMethod.PUT, assembleHeadersWithBody(command), responseType, urlVariables)
    }

    public <T> ResponseEntity<T> httpPut(String url, Object queryObject, Object command, Class<T> responseType, Object... urlVariables) {
        String query = uriHelper.buildQuery(url, queryObject).build().toUriString()
        restTemplate.exchange(query, HttpMethod.PUT, assembleHeadersWithBody(command), responseType, urlVariables)
    }

    public <T> ResponseEntity<T> httpPut(String url, Object command, ParameterizedTypeReference<T> responseType, Object... urlVariables) {
        restTemplate.exchange(url, HttpMethod.PUT, assembleHeadersWithBody(command), responseType, urlVariables)
    }

    public <T> ResponseEntity<T> httpPut(String url, Object queryObject, Object command, ParameterizedTypeReference<T> responseType, Object... urlVariables) {
        String query = uriHelper.buildQuery(url, queryObject).build().toUriString()
        restTemplate.exchange(query, HttpMethod.PUT, assembleHeadersWithBody(command), responseType, urlVariables)
    }

    public <T> ResponseEntity<T> httpPatch(String url, Object command, Class<T> responseType, Object... urlVariables) {
        restTemplate.exchange(url, HttpMethod.PATCH, assembleHeadersWithBody(command), responseType, urlVariables)
    }

    public <T> ResponseEntity<T> httpPatch(String url, Object queryObject, Object command, Class<T> responseType, Object... urlVariables) {
        String query = uriHelper.buildQuery(url, queryObject).build().toUriString()
        restTemplate.exchange(query, HttpMethod.PATCH, assembleHeadersWithBody(command), responseType, urlVariables)
    }

    public <T> ResponseEntity<T> httpPatch(String url, Object command, ParameterizedTypeReference<T> responseType, Object... urlVariables) {
        restTemplate.exchange(url, HttpMethod.PATCH, assembleHeadersWithBody(command), responseType, urlVariables)
    }

    public <T> ResponseEntity<T> httpPatch(String url, Object queryObject, Object command, ParameterizedTypeReference<T> responseType, Object... urlVariables) {
        String query = uriHelper.buildQuery(url, queryObject).build().toUriString()
        restTemplate.exchange(query, HttpMethod.PATCH, assembleHeadersWithBody(command), responseType, urlVariables)
    }

    public <T> ResponseEntity<T> httpDelete(String url, Class<T> responseType, Object... urlVariables) {
        restTemplate.exchange(url, HttpMethod.DELETE, assembleHeadersWithBody(), responseType, urlVariables)
    }

    public <T> ResponseEntity<T> httpDelete(String url, Object queryObject, Class<T> responseType, Object... urlVariables) {
        String query = uriHelper.buildQuery(url, queryObject).build().toUriString()
        restTemplate.exchange(query, HttpMethod.DELETE, assembleHeadersWithBody(), responseType, urlVariables)
    }

    private static HttpEntity<?> assembleHeadersWithBody(Object body = null) {
        HttpHeaders headers = new HttpHeaders()
        headers.setAccept([MediaType.APPLICATION_JSON])
        Tuple2<String, String> accountIdentityHeader = assembleAccountContextHeader()
        headers.set(accountIdentityHeader.first, accountIdentityHeader.second)
        new HttpEntity<?>(body, headers)
    }

    private static Tuple2<String, String> assembleAccountContextHeader(UUID accountId = ACCOUNT_ID, String accountRole = 'CLIENT') {
        String headerValue = AccountIdentity.builder()
                .id(accountId)
                .authority(new SimpleGrantedAuthority(accountRole))
                .build()
                .toHeaderString()
        new Tuple2<String, String>("x-account-id", headerValue)
    }
}
