package pl.com.rszewczyk.common.helper

import com.querydsl.core.types.dsl.BooleanOperation
import com.querydsl.core.types.dsl.StringPath
import pl.com.rszewczyk.common.querydsl.Predicates
import spock.lang.Specification

class PredicatesSpecIT extends Specification {
    static StringPath path = new StringPath("test")
    static String query = " a           "
    static String anotherQuery = "   b   "
    static String queryTrimmed = query.trim()
    static String anotherQueryTrimmed = anotherQuery.trim()

    def "should trim text on containsIgnoreCase"() {
        when:
            def pred = Predicates.builder().containsIgnoreCase(path, query)
        then:
            ((BooleanOperation)pred.predicates.first()).getArg(1).toString() == queryTrimmed
    }

    def "shouldn't trim text on containsIgnoreCaseNoTrim"() {
        when:
            def pred = Predicates.builder().containsIgnoreCaseNoTrim(path, query)
        then:
            ((BooleanOperation)pred.predicates.first()).getArg(1).toString() == query
    }
}
