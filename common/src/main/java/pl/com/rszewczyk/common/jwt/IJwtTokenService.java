package pl.com.rszewczyk.common.jwt;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.UUID;

public interface IJwtTokenService {

    <T> String encode(@NotNull UUID accountId, String permission, T body);

    JwtToken decode(@NotBlank String token);

    <T> JwtToken<T> decode(@NotBlank String token, @NotNull Class<T> clazz);
}
