package pl.com.rszewczyk.common.jwt.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import javax.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.com.rszewczyk.common.jwt.IJwtTokenService;
import pl.com.rszewczyk.common.jwt.JwtTokenServiceBean;

@Configuration
@EnableConfigurationProperties(JwtServiceConfiguration.Properties.class)
public class JwtServiceConfiguration {

  @Bean
  public IJwtTokenService jwtTokenService(Properties properties, ObjectMapper objectMapper) {
    return new JwtTokenServiceBean(properties, objectMapper);
  }

  @Data
  @Builder
  @NoArgsConstructor
  @AllArgsConstructor
  @ConfigurationProperties("jwt-token")
  public static class Properties {
    @NotBlank private String salt;
  }
}
