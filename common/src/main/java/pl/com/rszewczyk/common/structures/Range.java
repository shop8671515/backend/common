package pl.com.rszewczyk.common.structures;

import javax.annotation.Nullable;

public interface Range<T> {

    @Nullable
    T getFrom();

    @Nullable
    T getTo();
}
