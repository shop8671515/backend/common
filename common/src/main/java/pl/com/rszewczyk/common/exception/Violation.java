package pl.com.rszewczyk.common.exception;

import lombok.Value;

@Value
public class Violation {

  private final String field;

  private final String message;
}
