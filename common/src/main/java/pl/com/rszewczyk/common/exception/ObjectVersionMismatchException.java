package pl.com.rszewczyk.common.exception;

import pl.com.rszewczyk.common.ErrorCode;

public class ObjectVersionMismatchException extends RuntimeException implements IErrorCode {

  public ObjectVersionMismatchException(String message) {
    super(message);
  }

  public ObjectVersionMismatchException(String message, Throwable cause) {
    super(message, cause);
  }

  @Override
  public String getErrorCode() {
    return ErrorCode.VERSION_MISMATCH.name();
  }
}
