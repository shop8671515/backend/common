package pl.com.rszewczyk.common.exception;

import static java.util.Optional.ofNullable;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import pl.com.rszewczyk.common.ErrorCode;

public class ViolationException extends RuntimeException implements IErrorCode {
  private final Set<String> sourceErrorCodes;
  private final List<Violation> sourceViolations;
  private final Integer sourceHttpCode;

  public ViolationException(String message) {
    super(message);
    sourceErrorCodes = Collections.emptySet();
    sourceHttpCode = null;
    sourceViolations = null;
  }

  public ViolationException(String message, Throwable cause) {
    super(message, cause);
    sourceErrorCodes = Collections.emptySet();
    sourceHttpCode = null;
    sourceViolations = null;
  }

  public ViolationException(String message, Set<String> sourceErrorCodes) {
    super(message);
    this.sourceErrorCodes = ofNullable(sourceErrorCodes).orElse(Collections.emptySet());
    sourceHttpCode = null;
    sourceViolations = null;
  }

  public ViolationException(String message, Throwable cause, Set<String> sourceErrorCodes) {
    super(message, cause);
    this.sourceErrorCodes = ofNullable(sourceErrorCodes).orElse(Collections.emptySet());
    sourceHttpCode = null;
    sourceViolations = null;
  }

  public ViolationException(String message, Set<String> sourceErrorCodes, Integer sourceHttpCode) {
    super(message);
    this.sourceErrorCodes = ofNullable(sourceErrorCodes).orElse(Collections.emptySet());
    this.sourceHttpCode = sourceHttpCode;
    sourceViolations = null;
  }

  public ViolationException(
      String message, Throwable cause, Set<String> sourceErrorCodes, Integer sourceHttpCode) {
    super(message, cause);
    this.sourceErrorCodes = ofNullable(sourceErrorCodes).orElse(Collections.emptySet());
    this.sourceHttpCode = sourceHttpCode;
    sourceViolations = null;
  }

  public ViolationException(
      String message,
      Set<String> sourceErrorCodes,
      Integer sourceHttpCode,
      List<Violation> sourceViolations) {
    super(message);
    this.sourceErrorCodes = ofNullable(sourceErrorCodes).orElse(Collections.emptySet());
    this.sourceHttpCode = sourceHttpCode;
    this.sourceViolations = ofNullable(sourceViolations).orElse(Collections.emptyList());
  }

  public ViolationException(
      String message,
      Throwable cause,
      Set<String> sourceErrorCodes,
      Integer sourceHttpCode,
      List<Violation> sourceViolations) {
    super(message, cause);
    this.sourceErrorCodes = ofNullable(sourceErrorCodes).orElse(Collections.emptySet());
    this.sourceHttpCode = sourceHttpCode;
    this.sourceViolations = ofNullable(sourceViolations).orElse(Collections.emptyList());
  }

  @Override
  public String getErrorCode() {
    return ErrorCode.VIOLATION_EXCEPTION.name();
  }

  public Set<String> getSourceErrorCodes() {
    return sourceErrorCodes;
  }

  public Integer getSourceHttpCode() {
    return sourceHttpCode;
  }

  public List<Violation> getSourceViolations() {
    return sourceViolations;
  }
}
