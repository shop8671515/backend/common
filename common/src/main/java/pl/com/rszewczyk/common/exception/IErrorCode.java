package pl.com.rszewczyk.common.exception;

public interface IErrorCode {

  String getErrorCode();
}
