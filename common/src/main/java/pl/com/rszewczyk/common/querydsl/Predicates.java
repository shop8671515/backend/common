package pl.com.rszewczyk.common.querydsl;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.CollectionPathBase;
import com.querydsl.core.types.dsl.ComparableExpression;
import com.querydsl.core.types.dsl.NumberExpression;
import com.querydsl.core.types.dsl.SimpleExpression;
import com.querydsl.core.types.dsl.StringExpression;
import com.querydsl.core.types.dsl.StringPath;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import javax.annotation.Nullable;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import pl.com.rszewczyk.common.structures.Range;
import pl.com.rszewczyk.common.structures.ValuesInFilter;

@Getter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Predicates {

  private final List<Predicate> predicates = new ArrayList<>();

  public static Predicates builder() {
    return new Predicates();
  }

  public <T extends Comparable<T>> Predicates in(SimpleExpression<T> path, Collection<T> values) {
    addPredicate(path::in, values);
    return this;
  }

  public <C extends Collection<E>, E extends Comparable<E>, Q extends SimpleExpression<? super E>>
      Predicates in(CollectionPathBase<C, E, Q> path, Collection<E> values) {
    addPredicate(path.any()::in, values);
    return this;
  }

  public <T extends Comparable<T>> Predicates notIn(
      SimpleExpression<T> path, Collection<T> values) {
    addPredicate(path::notIn, values);
    return this;
  }

  public <T extends Comparable<T>> Predicates eq(SimpleExpression<T> path, T value) {
    addPredicate(path::eq, value);
    return this;
  }

  public <C extends Collection<E>, E extends Comparable<E>, Q extends SimpleExpression<? super E>>
      Predicates eq(CollectionPathBase<C, E, Q> path, Collection<E> values) {
    if (Objects.nonNull(values)) {
      this.predicates.add(
          values.stream()
              .map(path::contains)
              .reduce(path.size().eq(values.size()), BooleanExpression::and));
    }
    return this;
  }

  public <T extends Comparable<T>> Predicates ne(SimpleExpression<T> path, T value) {
    addPredicate(path::ne, value);
    return this;
  }

  /** Metoda wykonuje trim na zadanym napisie. */
  public Predicates like(StringExpression path, String value, LikeType type) {
    return likeNoTrim(path, StringUtils.trim(value), type);
  }

  /** Metoda NIE wykonuje trim na zadanym napisie. */
  public Predicates likeNoTrim(StringExpression path, String value, LikeType type) {
    addPredicate(v -> path.like(type.convert(v)), value);
    return this;
  }

  /** Metoda wykonuje trim na zadanym napisie. */
  public Predicates likeIgnoreCase(StringExpression path, String value, LikeType type) {
    return likeIgnoreCaseNoTrim(path, StringUtils.trim(value), type);
  }

  /** Metoda NIE wykonuje trim na zadanym napisie. */
  public Predicates likeIgnoreCaseNoTrim(StringExpression path, String value, LikeType type) {
    addPredicate(v -> path.likeIgnoreCase(type.convert(v)), value);
    return this;
  }

  public <E extends Comparable<E>, T extends ComparableExpression<E>> Predicates lt(
      T path, E value) {
    addPredicate(path::lt, value);
    return this;
  }

  public <E extends Comparable<E>, T extends ComparableExpression<E>> Predicates loe(
      T path, E value) {
    addPredicate(path::loe, value);
    return this;
  }

  public <E extends Comparable<E>, T extends ComparableExpression<E>> Predicates gt(
      T path, E value) {
    addPredicate(path::gt, value);
    return this;
  }

  public <E extends Comparable<E>, T extends ComparableExpression<E>> Predicates goe(
      T path, E value) {
    addPredicate(path::goe, value);
    return this;
  }

  public <E extends Number & Comparable<?>, T extends NumberExpression<E>> Predicates lt(
      T path, E value) {
    addPredicate(path::lt, value);
    return this;
  }

  public <E extends Number & Comparable<?>, T extends NumberExpression<E>> Predicates loe(
      T path, E value) {
    addPredicate(path::loe, value);
    return this;
  }

  public <E extends Number & Comparable<?>, T extends NumberExpression<E>> Predicates gt(
      T path, E value) {
    addPredicate(path::gt, value);
    return this;
  }

  public <E extends Number & Comparable<?>, T extends NumberExpression<E>> Predicates goe(
      T path, E value) {
    addPredicate(path::goe, value);
    return this;
  }

  public <E extends Comparable<?>, T extends ComparableExpression<E>> Predicates range(
      @NotNull T path, @Nullable Range<E> range) {
    if (Objects.nonNull(range)) {
      addPredicate(path::goe, range.getFrom());
      addPredicate(path::loe, range.getTo());
    }
    return this;
  }

  public <E extends Number & Comparable<?>, T extends NumberExpression<E>> Predicates range(
      @NotNull T path, @Nullable Range<E> range) {
    if (Objects.nonNull(range)) {
      addPredicate(path::goe, range.getFrom());
      addPredicate(path::loe, range.getTo());
    }
    return this;
  }

  /** Metoda wykonuje trim na zadanym napisie. */
  public Predicates containsIgnoreCase(StringPath path, String value) {
    return containsIgnoreCaseNoTrim(path, StringUtils.trim(value));
  }

  /** Metoda wykonuje trim na zadanym napisie. */
  public Predicates containsIgnoreCase(StringExpression path, String value) {
    return containsIgnoreCaseNoTrim(path, StringUtils.trim(value));
  }

  /** Metoda NIE wykonuje trim na zadanym napisie. */
  public Predicates containsIgnoreCaseNoTrim(StringPath path, String value) {
    addPredicate(path::containsIgnoreCase, value);
    return this;
  }

  /** Metoda NIE wykonuje trim na zadanym napisie. */
  public Predicates containsIgnoreCaseNoTrim(StringExpression path, String value) {
    addPredicate(path::containsIgnoreCase, value);
    return this;
  }

  /** Metoda wykonuje trim na zadanym napisie. */
  public Predicates containsIgnoreCase(Collection<StringPath> paths, String value) {
    return containsIgnoreCaseNoTrim(paths, StringUtils.trim(value));
  }

  /** Metoda NIE wykonuje trim na zadanym napisie. */
  public Predicates containsIgnoreCaseNoTrim(Collection<StringPath> paths, String value) {
    if (StringUtils.isNotBlank(value) && !CollectionUtils.isEmpty(paths)) {
      BooleanBuilder booleanBuilder = new BooleanBuilder();
      paths.forEach(i -> booleanBuilder.or(i.containsIgnoreCase(value)));
      this.predicates.add(booleanBuilder.getValue());
    }
    return this;
  }

  /** Metoda wykonuje trim na zadanych napisach. */
  public Predicates orContainsIgnoreCaseAny(
      Collection<StringPath> paths, Collection<String> values) {
    List<String> trimmedValues =
        CollectionUtils.emptyIfNull(values).stream()
            .map(StringUtils::trim)
            .filter(StringUtils::isNotEmpty)
            .collect(Collectors.toList());
    return orContainsIgnoreCaseAnyNoTrim(paths, trimmedValues);
  }

  /** Metoda nie wykonuje trim na zadanych napisach. */
  public Predicates orContainsIgnoreCaseAnyNoTrim(
      Collection<StringPath> paths, Collection<String> values) {
    if (CollectionUtils.isNotEmpty(values)) {
      BooleanBuilder booleanBuilder = new BooleanBuilder();
      paths.forEach(i -> values.forEach(v -> booleanBuilder.or(i.containsIgnoreCase(v))));
      this.predicates.add(booleanBuilder.getValue());
    }
    return this;
  }

  public Predicates orIn(Collection<StringPath> paths, Collection<String> values) {
    if (CollectionUtils.isNotEmpty(values)) {
      BooleanBuilder booleanBuilder = new BooleanBuilder();
      paths.forEach(i -> booleanBuilder.or(i.in(values)));
      this.predicates.add(booleanBuilder.getValue());
    }
    return this;
  }

  @Deprecated
  public <T> Predicates checkNull(SimpleExpression<T> path, Boolean value) {
    if (Boolean.FALSE.equals(value)) {
      this.predicate(path.isNull());
    } else if (Boolean.TRUE.equals(value)) {
      this.predicate(path.isNotNull());
    }
    return this;
  }

  public <T> Predicates isNull(SimpleExpression<T> path, Boolean value) {
    if (Boolean.FALSE.equals(value)) {
      this.predicate(path.isNotNull());
    } else if (Boolean.TRUE.equals(value)) {
      this.predicate(path.isNull());
    }
    return this;
  }

  public <T> Predicates isNotNull(SimpleExpression<T> path, Boolean value) {
    if (Boolean.FALSE.equals(value)) {
      this.predicate(path.isNull());
    } else if (Boolean.TRUE.equals(value)) {
      this.predicate(path.isNotNull());
    }
    return this;
  }

  /** in/notIn w oparciu o parametr filtra */
  public <T extends Comparable<?>> Predicates in(
      @NotNull SimpleExpression<T> path, @Nullable @Valid ValuesInFilter<T> valuesInFilter) {
    if (Objects.nonNull(valuesInFilter)) {
      if (Boolean.TRUE.equals(valuesInFilter.getExclude())) {
        addPredicate(path::notIn, valuesInFilter.getValues());
      } else {
        addPredicate(path::in, valuesInFilter.getValues());
      }
    }
    return this;
  }

  public Predicates predicate(Predicate predicate) {
    if (Objects.nonNull(predicate)) {
      this.predicates.add(predicate);
    }
    return this;
  }

  public Predicates add(Predicates predicate) {
    if (Objects.nonNull(predicate)) {
      this.predicates.addAll(predicate.predicates);
    }
    return this;
  }

  public Predicate build() {
    return Optional.ofNullable(ExpressionUtils.allOf(this.predicates)).orElse(new BooleanBuilder());
  }

  private <T> void addPredicate(
      Function<Collection<T>, Predicate> expression, Collection<T> values) {
    if (CollectionUtils.isNotEmpty(values)) {
      this.predicates.add(expression.apply(values));
    }
  }

  private <T> void addPredicate(Function<T, Predicate> expression, T value) {
    if (Objects.nonNull(value)) {
      this.predicates.add(expression.apply(value));
    }
  }

  private void addPredicate(Function<String, Predicate> expression, String value) {
    if (StringUtils.isNotBlank(value)) {
      this.predicates.add(expression.apply(value));
    }
  }

  @Getter
  @RequiredArgsConstructor
  public enum LikeType {
    BOTH("%%%s%%"),
    LEFT("%%%s"),
    RIGHT("%s%%");

    private final String template;

    public String convert(String value) {
      return String.format(this.template, value);
    }
  }
}
