package pl.com.rszewczyk.common.jwt;

import io.jsonwebtoken.Claims;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@Builder(toBuilder = true)
public class JwtToken<T> {

    @NotNull
    private UUID accountId;

    private String permission;

    private T body;

    @NotNull
    private Claims raw;
}
