package pl.com.rszewczyk.common.jwt;

public class InvalidTokenException extends RuntimeException {

    public InvalidTokenException(Throwable cause) {
        super(String.format("Invalid JWT token (%s)", cause.getMessage()), cause);
    }
}
