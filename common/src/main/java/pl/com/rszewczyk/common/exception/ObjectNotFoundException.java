package pl.com.rszewczyk.common.exception;

import java.util.Objects;
import pl.com.rszewczyk.common.ErrorCode;

public class ObjectNotFoundException extends RuntimeException implements IErrorCode {
  private static final String DEFAULT_ERROR_CODE = ErrorCode.ITEM_NOT_FOUND.name();
  private String errorCode;

  public ObjectNotFoundException(String message) {
    super(message);
  }

  public ObjectNotFoundException(String message, String errorCode) {
    super(message);
    this.errorCode = errorCode;
  }

  public ObjectNotFoundException(String message, Throwable cause) {
    super(message, cause);
  }

  public ObjectNotFoundException(String message, String errorCode, Throwable cause) {
    super(message, cause);
    this.errorCode = errorCode;
  }

  @Override
  public String getErrorCode() {
    return Objects.nonNull(errorCode) ? errorCode : DEFAULT_ERROR_CODE;
  }
}
