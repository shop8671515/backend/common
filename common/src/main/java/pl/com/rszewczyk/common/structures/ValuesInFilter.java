package pl.com.rszewczyk.common.structures;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.Collection;

public interface ValuesInFilter<T> {

    @NotNull
    Collection<T> getValues();

    @Nullable
    Boolean getExclude();
}
