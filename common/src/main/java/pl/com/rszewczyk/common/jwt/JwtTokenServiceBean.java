package pl.com.rszewczyk.common.jwt;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;

import javax.crypto.SecretKey;
import java.util.UUID;
import pl.com.rszewczyk.common.jwt.configuration.JwtServiceConfiguration;

@Slf4j
@Validated
@RequiredArgsConstructor
public class JwtTokenServiceBean implements IJwtTokenService {
    private static final String BODY_PLACEHOLDER = "body";
    private final JwtServiceConfiguration.Properties properties;
    private final ObjectMapper objectMapper;

    @Override
    public <T> String encode(UUID accountId, String permission, T body) {
        return Jwts.builder()
                .setSubject(accountId.toString())
                .signWith(assembleSecretKey())
                .claim(BODY_PLACEHOLDER, body)
                .setAudience(permission)
                .compact();
    }

    @Override
    public JwtToken decode(String token) {
        try {
            Claims claims = Jwts.parser().setSigningKey(assembleSecretKey()).parseClaimsJws(token).getBody();
            return JwtToken.builder()
                    .accountId(UUID.fromString(claims.getSubject()))
                    .permission(claims.getAudience())
                    .raw(claims)
                    .build();
        } catch (Exception e) {
            throw new InvalidTokenException(e);
        }
    }

    @Override
    public <T> JwtToken<T> decode(String token, Class<T> clazz) {
        JwtToken<T> jwtToken = decode(token);
        return jwtToken.toBuilder()
                .body(objectMapper.convertValue(jwtToken.getRaw().get(BODY_PLACEHOLDER), clazz))
                .build();
    }

    private SecretKey assembleSecretKey() {
        return Keys.hmacShaKeyFor(properties.getSalt().getBytes());
    }
}
