CREATE TABLE IF NOT EXISTS named_object_snap (
    id   UUID NOT NULL,
    "type" TEXT NOT NULL,
    name TEXT NOT NULL,
    PRIMARY KEY (id, "type")
);

INSERT INTO named_object_snap VALUES ('00000000-0000-0000-0000-000000000000', 'USER', 'system') ON CONFLICT DO NOTHING;
INSERT INTO named_object_snap VALUES ('00000000-0000-0000-0000-000000000001', 'USER', 'anonymous') ON CONFLICT DO NOTHING;

UPDATE named_object_snap SET name = 'system' WHERE id = '00000000-0000-0000-0000-000000000000' AND "type" = 'USER';
UPDATE named_object_snap SET name = 'anonymous' WHERE id = '00000000-0000-0000-0000-000000000001' AND "type" = 'USER';

ALTER TABLE named_object_snap ADD COLUMN IF NOT EXISTS deleted BOOLEAN;