package pl.com.rszewczyk.common.named.object.repository;

import org.hibernate.Hibernate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;
import pl.com.rszewczyk.common.named.object.entity.NamedObjectId;
import pl.com.rszewczyk.common.named.object.entity.NamedObjectSnap;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Repository
public interface NamedObjectSnapRepository extends JpaRepository<NamedObjectSnap, NamedObjectId>, QuerydslPredicateExecutor<NamedObjectSnap> {

    Optional<NamedObjectSnap> findByCid(@NotNull @Valid NamedObjectId id);

    List<NamedObjectSnap> findByCidType(@NotNull @Valid String type);

    default Optional<NamedObjectSnap> findByIdAndType(@NotNull UUID id, @NotBlank String type) {
        return findByCid(new NamedObjectId(id, type))
                .map(it -> (NamedObjectSnap) Hibernate.unproxy(it));
    }

    default Set<NamedObjectSnap> findByIdsAndType(@NotNull Set<UUID> ids, @NotBlank String type) {
        Set<NamedObjectId> namedObjectIds = ids.stream()
                .map(id -> new NamedObjectId(id, type))
                .collect(Collectors.toSet());
        return findAllById(namedObjectIds).stream()
                .map(it -> (NamedObjectSnap) Hibernate.unproxy(it))
                .collect(Collectors.toSet());
    }
}
