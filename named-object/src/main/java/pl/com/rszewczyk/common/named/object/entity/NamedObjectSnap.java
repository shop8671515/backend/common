package pl.com.rszewczyk.common.named.object.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.validation.constraints.NotBlank;
import java.util.UUID;

@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "cid")
public class NamedObjectSnap {

    @EmbeddedId
    private NamedObjectId cid = new NamedObjectId();

    @NotBlank
    private String name;

    private Boolean deleted;

    public UUID getId() {
        return cid.getId();
    }
}
