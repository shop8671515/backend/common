package pl.com.rszewczyk.common.named.object.mapping;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.mapstruct.Mapper;
import static org.mapstruct.ReportingPolicy.ERROR;
import pl.com.rszewczyk.common.named.object.entity.NamedObjectSnap;
import pl.com.rszewczyk.common.api.support.NamedObject;

@Mapper(unmappedTargetPolicy = ERROR)
public interface NamedObjectMapper {

    NamedObject namedObject(@NotNull @Valid NamedObjectSnap namedObjectSnap);
}
