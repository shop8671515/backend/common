package pl.com.rszewczyk.common.named.object.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.UUID;

@Data
@Embeddable
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class NamedObjectId implements Serializable {

    @NotNull
    @Column(columnDefinition = "uuid", updatable = false)
    private UUID id;

    @NotBlank
    private String type;
}
