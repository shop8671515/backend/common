package pl.com.rszewczyk.common.named.object.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import pl.com.rszewczyk.common.named.object.mapping.NamedObjectMapper;

@Configuration
@Import({NamedObjectMapper.class})
@EntityScan(basePackages = "pl.com.bit.common.named.object.entity")
@EnableJpaRepositories("pl.com.bit.common.named.object.repository")
public class NamedObjectAutoConfig {}
