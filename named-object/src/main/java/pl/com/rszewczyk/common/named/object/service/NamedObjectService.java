package pl.com.rszewczyk.common.named.object.service;

import pl.com.rszewczyk.common.named.object.entity.NamedObjectSnap;

import javax.validation.constraints.NotNull;
import java.util.UUID;

public interface NamedObjectService {

    NamedObjectSnap get(@NotNull UUID id);
}
