package pl.com.rszewczyk.common.named.object.service;

import org.springframework.beans.factory.annotation.Autowired;
import pl.com.rszewczyk.common.named.object.entity.NamedObjectSnap;
import pl.com.rszewczyk.common.named.object.repository.NamedObjectSnapRepository;

import javax.validation.constraints.NotNull;
import java.util.UUID;

public abstract class NamedObjectAbstractService implements NamedObjectService {
    @Autowired
    private NamedObjectSnapRepository namedObjectSnapRepository;

    protected abstract String getType();

    public abstract NamedObjectSnap requestObject(@NotNull UUID id);

    @Override
    public NamedObjectSnap get(UUID id) {
        return namedObjectSnapRepository.findByIdAndType(id, getType()).orElseGet(() -> namedObjectSnapRepository.save(requestObject(id)));
    }
}
