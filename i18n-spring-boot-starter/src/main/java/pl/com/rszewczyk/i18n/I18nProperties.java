package pl.com.rszewczyk.i18n;

import java.util.List;
import java.util.Locale;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "i18n")
public class I18nProperties {
  private String basename;

  @NotNull
  @Size(min = 1)
  private List<Locale> supportedLocales;

  private int cacheSeconds;

  @NotNull private Locale defaultLocale;

  @NotNull private Boolean useCodeAsDefaultMessage = false;
}
