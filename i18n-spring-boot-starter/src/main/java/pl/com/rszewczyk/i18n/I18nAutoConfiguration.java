package pl.com.rszewczyk.i18n;

import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.validation.ValidationAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@AutoConfigureBefore(ValidationAutoConfiguration.class)
public class I18nAutoConfiguration {

  @Configuration
  @Import({MessageSourceConfiguration.class, I18nProperties.class})
  public static class I18nMessageSourceConfiguration {}
}
