package pl.com.rszewczyk.i18n;

import java.util.stream.Stream;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.util.StringUtils;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.AcceptHeaderLocaleResolver;

@Slf4j
@Configuration
public class MessageSourceConfiguration {
  @Autowired private I18nProperties i18nProperties;

  @Bean
  public MessageSource messageSource() {
    String[] basenames =
        Stream.of(i18nProperties.getBasename(), "classpath:messages")
            .filter(StringUtils::hasText)
            .toArray(String[]::new);

    ReloadableResourceBundleMessageSource messageSource =
        new ReloadableResourceBundleMessageSource();
    messageSource.setDefaultEncoding("utf8");
    messageSource.setBasenames(basenames);
    messageSource.setUseCodeAsDefaultMessage(i18nProperties.getUseCodeAsDefaultMessage());
    messageSource.setCacheSeconds(i18nProperties.getCacheSeconds());
    return messageSource;
  }

  @Bean
  public MessageSourceAccessor messageSourceAccessor() {
    return new MessageSourceAccessor(messageSource());
  }

  @Bean
  public LocaleResolver localeResolver() {
    AcceptHeaderLocaleResolver localeResolver = new AcceptHeaderLocaleResolver();
    localeResolver.setSupportedLocales(i18nProperties.getSupportedLocales());
    localeResolver.setDefaultLocale(i18nProperties.getDefaultLocale());
    return localeResolver;
  }

  @Bean
  @Primary
  public LocalValidatorFactoryBean validator() {
    LocalValidatorFactoryBean factory = new LocalValidatorFactoryBean();
    factory.setValidationMessageSource(messageSource());
    return factory;
  }

  @Configuration
  @ConditionalOnWebApplication
  public class ValidationMessageConfiguration implements WebMvcConfigurer {
    @Override
    public Validator getValidator() {
      return validator();
    }
  }
}
