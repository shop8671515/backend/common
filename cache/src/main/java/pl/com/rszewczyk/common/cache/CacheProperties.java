package pl.com.rszewczyk.common.cache;

import java.util.Map;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

@Data
@Validated
@Configuration
@NoArgsConstructor
@ConfigurationProperties("bit")
public class CacheProperties {
  @NotNull private CacheSpecType defaultCacheType = CacheSpecType.IN_MEMORY;

  @NotNull private Map<String, CacheSpec> caches;
}
