package pl.com.rszewczyk.common.cache;

import java.time.Duration;
import java.util.Map;
import java.util.stream.Collectors;
import javax.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RedissonClient;
import org.redisson.codec.JsonJacksonCodec;
import org.redisson.spring.cache.CacheConfig;
import org.redisson.spring.cache.RedissonSpringCacheManager;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
@RequiredArgsConstructor
public class DistributedCacheConfiguration {
  private final CacheSpecService cacheSpecService;
  private final CacheKeysProvider cacheKeysProvider;
  private final RedissonClient redissonClient;

  @Bean
  public CacheManager distributedCacheManager() {
    log.info("Creating distributedCacheManager {cacheKeys: {}}", cacheKeysProvider.getCacheKeys());
    Map<String, CacheConfig> config =
        cacheKeysProvider.getCacheKeys().stream()
            .map(
                key ->
                    CacheSpecWithKey.builder()
                        .cacheKey(key)
                        .spec(cacheSpecService.assembleSpec(key))
                        .build())
            .filter(spec -> spec.getSpec().getType() == CacheSpecType.DISTRIBUTED)
            .collect(Collectors.toMap(CacheSpecWithKey::getCacheKey, this::assembleConfig));

    RedissonSpringCacheManager cacheManager = new RedissonSpringCacheManager(redissonClient);
    cacheManager.setConfig(config);
    cacheManager.setCodec(new JsonJacksonCodec());
    return cacheManager;
  }

  private CacheConfig assembleConfig(@NotNull CacheSpecWithKey spec) {
    log.info("Assembling cache {spec: {}}", spec);
    long ttl = Duration.ofSeconds(spec.getSpec().getExpireAfterWriteSeconds()).toMillis();
    return new CacheConfig(ttl, ttl);
  }
}
