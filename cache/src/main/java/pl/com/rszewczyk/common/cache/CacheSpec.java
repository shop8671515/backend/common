package pl.com.rszewczyk.common.cache;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CacheSpec {
  private Long expireAfterWriteSeconds;
  private Long maximumSize;

  @Builder.Default private CacheSpecType type = CacheSpecType.IN_MEMORY;
}
