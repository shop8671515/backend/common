package pl.com.rszewczyk.common.cache;

import com.github.benmanes.caffeine.cache.CaffeineSpec;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RedissonClient;
import org.redisson.spring.starter.RedissonAutoConfiguration;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.jcache.JCacheCacheManager;
import org.springframework.cache.support.CompositeCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;
import pl.com.rszewczyk.common.cache.handler.DefaultCacheEvictHandler;

@Slf4j
@Configuration
@EnableCaching
@AllArgsConstructor
public class CacheAutoConfiguration {

  @ConditionalOnClass(CaffeineSpec.class)
  @ConditionalOnProperty(value = "bit.cache-type", havingValue = "IN_MEMORY", matchIfMissing = true)
  @Import({
    InMemoryCacheConfiguration.class,
    CacheSpecService.class,
    CacheProperties.class,
    CacheHelper.class,
    DefaultCacheEvictHandler.class
  })
  @Configuration
  public static class InMemoryCacheAutoConfiguration {}

  @AutoConfigureBefore(RedissonAutoConfiguration.class)
  @ConditionalOnBean(RedissonClient.class)
  @ConditionalOnProperty(value = "bit.cache-type", havingValue = "DISTRIBUTED")
  @Import({
    DistributedCacheConfiguration.class,
    CacheSpecService.class,
    CacheProperties.class,
    CacheHelper.class
  })
  @Configuration
  public static class DistributedCacheAutoConfiguration {}

  @Bean
  public CacheKeysProvider cacheKeysProvider(CacheProperties cacheProperties) {
    return () -> cacheProperties.getCaches().keySet();
  }

  @Bean
  @Primary
  @ConditionalOnBean(CacheManager.class)
  public CacheManager compositeCacheManager(@NotNull List<CacheManager> cacheManagers) {
    log.trace(
        "Creating compositeCacheManager {cacheManagers: {}}",
        Arrays.deepToString(cacheManagers.toArray()));
    List<CacheManager> filteredCacheManagers =
        cacheManagers.stream().filter(this::notRedissonJCacheManager).collect(Collectors.toList());

    CompositeCacheManager compositeCacheManager = new CompositeCacheManager();
    compositeCacheManager.setCacheManagers(filteredCacheManagers);
    log.info(
        "Created compositeCacheManager {cacheManagers: {}}",
        Arrays.deepToString(filteredCacheManagers.toArray()));
    return compositeCacheManager;
  }

  private boolean notRedissonJCacheManager(CacheManager cacheManager) {
    if (cacheManager instanceof JCacheCacheManager) {
      JCacheCacheManager jCacheCacheManager = (JCacheCacheManager) cacheManager;
      return !(jCacheCacheManager.getCacheManager() instanceof JCacheCacheManager);
    }
    return true;
  }
}
