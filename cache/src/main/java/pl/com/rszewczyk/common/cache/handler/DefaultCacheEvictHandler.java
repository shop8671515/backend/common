package pl.com.rszewczyk.common.cache.handler;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.support.ErrorMessage;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;
import pl.com.rszewczyk.common.cache.CacheEvictEvent;
import pl.com.rszewczyk.common.cache.CacheEvictStream;

import javax.annotation.Nullable;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Objects;

@Slf4j
@Validated
@Component
@RequiredArgsConstructor
@EnableBinding(CacheEvictStream.Sink.class)
public class DefaultCacheEvictHandler {
    private final CacheManager cacheManager;

    @StreamListener(CacheEvictStream.Sink.INPUT)
    public void handle(@NotNull @Valid CacheEvictEvent evictEvent) {
        log.trace("Evicting cache entries: {}", evictEvent);
        if (CollectionUtils.isEmpty(evictEvent.getCacheNames())) {
            log.trace("No cache names provided to clear");
            return;
        }
        evictEvent.getCacheNames().stream()
                .map(cacheManager::getCache)
                .filter(Objects::nonNull)
                .forEach(c -> evictEntries(c, evictEvent.getKeys()));
        log.trace("Cache entries evicted {event: {}}", evictEvent);
    }

    @ServiceActivator(inputChannel = CacheEvictStream.Sink.INPUT)
    public void error(ErrorMessage errorMessage) {
        log.warn("Error when processing event {}", errorMessage);
        throw new DefaultCacheEvictEventException(errorMessage.toString());
    }

    private void evictEntries(@NotNull Cache c, @Nullable List<Object> keys) {
        if (Objects.isNull(keys)) {
            c.clear();
        } else {
            keys.forEach(c::evict);
        }
    }
}
