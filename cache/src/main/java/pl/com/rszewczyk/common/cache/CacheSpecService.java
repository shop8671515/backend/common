package pl.com.rszewczyk.common.cache;

import static java.util.Optional.ofNullable;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class CacheSpecService {
  private final CacheProperties cacheProperties;
  static final String DEFAULT_CACHE_KEY = "default";
  static final CacheSpec DEFAULT_CACHE_SPEC =
      CacheSpec.builder()
          .expireAfterWriteSeconds(60L)
          .maximumSize(10000L)
          .type(CacheSpecType.IN_MEMORY)
          .build();

  public CacheSpec assembleSpec(@Nullable String key) {
    log.trace("Assembling cache spec {key: {}}", key);
    CacheSpec spec;
    if (DEFAULT_CACHE_KEY.equals(key)) {
      spec =
          merge(
              cacheProperties.getCaches().getOrDefault(DEFAULT_CACHE_KEY, DEFAULT_CACHE_SPEC),
              DEFAULT_CACHE_SPEC);
    } else {
      CacheSpec defaultSpec = assembleSpec(DEFAULT_CACHE_KEY);
      spec = merge(cacheProperties.getCaches().getOrDefault(key, defaultSpec), defaultSpec);
    }
    log.debug("Assembled cache spec {key: {}, spec: {}}", key, spec);
    return spec;
  }

  private CacheSpec merge(@NotNull CacheSpec from1, @NotNull CacheSpec from2) {
    return CacheSpec.builder()
        .expireAfterWriteSeconds(
            ofNullable(from1.getExpireAfterWriteSeconds())
                .orElseGet(from2::getExpireAfterWriteSeconds))
        .maximumSize(ofNullable(from1.getMaximumSize()).orElseGet(from2::getMaximumSize))
        .type(ofNullable(from1.getType()).orElseGet(from2::getType))
        .build();
  }
}
