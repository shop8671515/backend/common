package pl.com.rszewczyk.common.cache;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.interceptor.SimpleKeyGenerator;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

@Component
@Validated
@RequiredArgsConstructor
public class CacheHelper {
  private final CacheManager cacheManager;

  public <T> T cache(
      @NotNull Supplier<T> supplier, @NotBlank String cacheKey, @NotNull Object... args) {
    return Objects.requireNonNull(cacheManager.getCache(cacheKey))
        .get(SimpleKeyGenerator.generateKey(args), supplier::get);
  }

  public <K, V> Set<V> caches(
      @NotNull Function<Set<K>, Map<K, V>> supplier,
      @NotBlank String cacheKey,
      @NotNull Class<V> type,
      @NotNull Set<K> keys) {
    Cache cache = cacheManager.getCache(cacheKey);
    Map<K, V> fromCache = new HashMap<>();

    if (Objects.isNull(cache)) {
      throw new NullPointerException("Cache cannot be null!");
    }

    keys.forEach(id -> fromCache.put(id, cache.get(SimpleKeyGenerator.generateKey(id), type)));

    Set<K> load =
        fromCache.entrySet().stream()
            .filter(entry -> Objects.isNull(entry.getValue()))
            .map(Map.Entry::getKey)
            .collect(Collectors.toSet());

    if (!load.isEmpty()) {
      Map<K, V> loaded = supplier.apply(load);
      loaded.forEach(
          (elementId, element) -> cache.put(SimpleKeyGenerator.generateKey(elementId), elementId));
      fromCache.putAll(loaded);
    }
    return fromCache.values().stream().filter(Objects::nonNull).collect(Collectors.toSet());
  }

  public <T> T get(@NotNull Class<T> type, @NotBlank String cacheKey, @NotNull Object... args) {
    Cache cache = cacheManager.getCache(cacheKey);
    if (Objects.isNull(cache)) {
      throw new NullPointerException("Cache cannot be null!");
    }
    return cache.get(SimpleKeyGenerator.generateKey(args), type);
  }

  public <K, V> Set<V> get(
      @NotNull Class<V> type, @NotNull Set<K> keys, @NotBlank String cacheKey) {
    Cache cache = cacheManager.getCache(cacheKey);
    if (Objects.isNull(cache)) {
      throw new NullPointerException("Cache cannot be null!");
    }
    return keys.stream()
        .map(it -> cache.get(SimpleKeyGenerator.generateKey(it), type))
        .filter(Objects::nonNull)
        .collect(Collectors.toSet());
  }

  public <T> void put(T data, @NotBlank String cacheKey, @NotNull Object... args) {
    Cache cache = cacheManager.getCache(cacheKey);
    if (Objects.isNull(cache)) {
      throw new NullPointerException("Cache cannot be null!");
    }
    cache.put(SimpleKeyGenerator.generateKey(args), data);
  }

  public void evict(@NotBlank String cacheKey, @NotNull Object... args) {
    Cache cache = cacheManager.getCache(cacheKey);
    if (Objects.isNull(cache)) {
      throw new NullPointerException("Cache cannot be null!");
    }
    cache.evict(SimpleKeyGenerator.generateKey(args));
  }

  public void clear(@NotBlank String cacheKey) {
    Cache cache = cacheManager.getCache(cacheKey);
    if (Objects.isNull(cache)) {
      throw new NullPointerException("Cache cannot be null!");
    }
    cache.clear();
  }
}
