package pl.com.rszewczyk.common.cache;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;

import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class CacheEvictEvent {
    private List<Object> keys;

    @NotEmpty
    private List<String> cacheNames;

    public Message<CacheEvictEvent> toMessage() {
        return MessageBuilder.withPayload(this)
                .build();
    }
}
