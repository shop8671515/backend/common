package pl.com.rszewczyk.common.cache;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

public interface CacheEvictStream {
    String NAME = "cache-evict-stream";

    interface Source {
        String OUTPUT = NAME;

        @Output(OUTPUT)
        MessageChannel output();
    }

    interface Sink {
        String INPUT = NAME + "-input";

        @Input(INPUT)
        SubscribableChannel input();
    }
}
