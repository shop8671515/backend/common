package pl.com.rszewczyk.common.cache;

import com.github.benmanes.caffeine.cache.Caffeine;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import javax.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.caffeine.CaffeineCache;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
@RequiredArgsConstructor
public class InMemoryCacheConfiguration {
  private final CacheSpecService commonCacheSpecService;

  private final CacheKeysProvider cacheKeysProvider;

  @Bean
  public CacheManager cacheManager() {
    log.info("Creating inMemoryCacheManager {cacheKeys: {}}", cacheKeysProvider.getCacheKeys());
    Set<Cache> caches =
        cacheKeysProvider.getCacheKeys().stream()
            .map(
                key ->
                    CacheSpecWithKey.builder()
                        .cacheKey(key)
                        .spec(commonCacheSpecService.assembleSpec(key))
                        .build())
            .filter(spec -> spec.getSpec().getType() == CacheSpecType.IN_MEMORY)
            .map(this::assembleCache)
            .collect(Collectors.toSet());

    SimpleCacheManager simpleCacheManager = new SimpleCacheManager();
    simpleCacheManager.setCaches(caches);
    return simpleCacheManager;
  }

  private Cache assembleCache(@NotNull CacheSpecWithKey spec) {
    log.info("Assembling cache {spec: {}}", spec);
    return new CaffeineCache(
        spec.getCacheKey(),
        Caffeine.newBuilder()
            .expireAfterWrite(spec.getSpec().getExpireAfterWriteSeconds(), TimeUnit.SECONDS)
            .maximumSize(spec.getSpec().getMaximumSize())
            .build());
  }
}
