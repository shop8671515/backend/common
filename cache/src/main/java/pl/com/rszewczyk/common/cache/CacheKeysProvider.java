package pl.com.rszewczyk.common.cache;

import java.util.Collection;

public interface CacheKeysProvider {

  Collection<String> getCacheKeys();
}
