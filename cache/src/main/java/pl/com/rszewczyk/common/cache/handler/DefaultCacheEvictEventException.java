package pl.com.rszewczyk.common.cache.handler;

import org.springframework.http.HttpStatus;
import pl.com.rszewczyk.common.exception.IErrorCode;

import javax.annotation.Nullable;
import pl.com.rszewczyk.http.problem.ProblemLogLevel;
import pl.com.rszewczyk.http.problem.ProblemMetadataAware;

public class DefaultCacheEvictEventException extends RuntimeException implements IErrorCode, ProblemMetadataAware {
    private static final String ERROR_CODE = "CACHE_EVICT_EVENT_HANDLE_FAILED";

    public DefaultCacheEvictEventException(@Nullable String message) {
        super(message);
    }

    @Override
    public String getErrorCode() {
        return ERROR_CODE;
    }

    @Override
    public HttpStatus getHttpStatus() {
        return HttpStatus.INTERNAL_SERVER_ERROR;
    }

    @Override
    public ProblemLogLevel getLogLevel() {
        return ProblemLogLevel.ERROR;
    }
}
