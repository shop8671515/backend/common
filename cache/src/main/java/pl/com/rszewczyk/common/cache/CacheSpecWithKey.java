package pl.com.rszewczyk.common.cache;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CacheSpecWithKey {
  private String cacheKey;
  private CacheSpec spec;
}
