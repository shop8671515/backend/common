package pl.com.rszewczyk.common.cache;

public enum CacheSpecType {
  IN_MEMORY,
  DISTRIBUTED
}
