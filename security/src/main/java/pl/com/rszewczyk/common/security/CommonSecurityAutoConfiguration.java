package pl.com.rszewczyk.common.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import pl.com.rszewczyk.common.security.api.AccountIdentity;
import pl.com.rszewczyk.common.security.api.AccountIdentityService;
import pl.com.rszewczyk.common.security.api.SecurityApiAutoConfiguration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static java.util.Optional.ofNullable;

@Configuration
@Import({
        SecurityApiAutoConfiguration.class
})
@EnableJpaAuditing
public class CommonSecurityAutoConfiguration {

    @Bean
    AccountIdentityAuditorAware auditorProvider() {
        return new AccountIdentityAuditorAware();
    }

    @Configuration
    public class ArgumentResolverWebMvcConfig implements WebMvcConfigurer {
        @Autowired
        private AccountIdentityAuditorAware accountIdentityAuditorAware;

        @Autowired
        private AccountIdentityService accountIdentityService;

        @Override
        public void addInterceptors(InterceptorRegistry registry) {
            registry.addInterceptor(new HandlerInterceptor() {
                @Override
                public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
                    accountIdentityAuditorAware.setCurrentAuditorId(ofNullable(accountIdentityService.resolve(request)).map(AccountIdentity::getId).orElse(null));
                    return true;
                }

                @Override
                public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
                    accountIdentityAuditorAware.cleanCurrentAuditorId();
                }

                @Override
                public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
                    accountIdentityAuditorAware.cleanCurrentAuditorId();
                }
            });
        }
    }
}
