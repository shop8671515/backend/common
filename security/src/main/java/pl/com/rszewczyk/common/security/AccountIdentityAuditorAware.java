package pl.com.rszewczyk.common.security;

import com.google.common.base.MoreObjects;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.AuditorAware;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;
import java.util.Optional;
import java.util.UUID;

import static com.google.common.base.MoreObjects.firstNonNull;
import static java.util.Optional.ofNullable;

@Slf4j
public class AccountIdentityAuditorAware implements AuditorAware<UUID> {
    public static final String FALLBACK_ID_PROPERTY_NAME = "bit.auditor.fallbackAuditorUUID";

    @Value("${" + FALLBACK_ID_PROPERTY_NAME + ":#{null}}")
    private UUID fallbackAuditorId;

    @PostConstruct
    private void init() {
        if (fallbackAuditorId == null) {
            log.warn("No fallback auditor id set ({}) -> modifications to audited entities require auditor settings", FALLBACK_ID_PROPERTY_NAME);
        }
    }

    private final ThreadLocal<UUID> threadLocal = new ThreadLocal<>();

    void cleanCurrentAuditorId() {
        threadLocal.remove();
    }

    void setCurrentAuditorId(@NotNull UUID currentAuditorId) {
        log.trace("Setting auditor id: {}", currentAuditorId);
        threadLocal.set(currentAuditorId);
    }

    @Override
    public Optional<UUID> getCurrentAuditor() {
        return ofNullable(firstNonNull(threadLocal.get(), fallbackAuditorId));
    }
}
