package pl.com.rszewczyk.common.api.support.paging;

import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

public class PageUtils {

  public static <S, D> DeserializablePage<D> map(
      DeserializablePage<S> page, Function<Collection<S>, List<D>> mapper) {
    return DeserializablePage.fromPage(map((Page<S>) page, mapper));
  }

  public static <S, D> Page<D> map(Page<S> page, Function<Collection<S>, List<D>> mapper) {
    boolean isPageable = page.getNumber() >= 0 && page.getSize() >= 1;
    return new PageImpl<>(
        mapper.apply(page.getContent()),
        isPageable ? PageRequest.of(page.getNumber(), page.getSize(), page.getSort()) : null,
        page.getTotalElements());
  }
}
