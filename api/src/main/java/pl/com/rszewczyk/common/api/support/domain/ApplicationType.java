package pl.com.rszewczyk.common.api.support.domain;

import lombok.extern.slf4j.Slf4j;

import java.util.Optional;

@Slf4j
public enum ApplicationType {

    PUBLIC("public-api-gateway"),
    OPERATOR("operator-api-gateway"),
    MOBILE("mobile-api-gateway"),
    UNKNOWN("");

    public static final String X_APPLICATION_NAME = "x-application-name";
    private final String xApplicationName;

    ApplicationType(String xApplicationName) {
        this.xApplicationName = xApplicationName;
    }

    public static ApplicationType resolve(String headerValue) {
        Optional<ApplicationType> applicationType = ofHeader(headerValue);
        if (applicationType.isEmpty()) {
            log.warn("Application type cannot be resolved from header " + headerValue);
        }
        return applicationType.orElse(ApplicationType.UNKNOWN);
    }

    public static Optional<ApplicationType> ofHeader(String headerValue) {
        for (ApplicationType ap : values()) {
            if (ap.xApplicationName.equals(headerValue) || ap.name().equals(headerValue)) {
                return Optional.of(ap);
            }
        }
        return Optional.empty();
    }
}
