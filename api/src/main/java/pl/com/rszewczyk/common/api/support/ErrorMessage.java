package pl.com.rszewczyk.common.api.support;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.validation.constraints.NotBlank;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
public class ErrorMessage {

  @NotBlank private String code;

  @NotBlank private String message;

  public ErrorMessage(@JsonProperty("code") String code, @JsonProperty("message") String message) {
    this.code = code;
    this.message = message;
  }
}
