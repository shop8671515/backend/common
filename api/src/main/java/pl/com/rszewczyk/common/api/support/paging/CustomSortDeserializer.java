package pl.com.rszewczyk.common.api.support.paging;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.data.domain.Sort;

public class CustomSortDeserializer extends JsonDeserializer<Sort> {

  @Override
  public Sort deserialize(JsonParser jp, DeserializationContext context) throws IOException {
    ArrayNode node = jp.getCodec().readTree(jp);
    List<Sort.Order> orders = new ArrayList<>(node.size());
    for (JsonNode obj : node) {
      orders.add(
          new Sort.Order(
              Sort.Direction.valueOf(obj.get("direction").asText()), obj.get("property").asText()));
    }
    return Sort.by(orders);
  }
}
