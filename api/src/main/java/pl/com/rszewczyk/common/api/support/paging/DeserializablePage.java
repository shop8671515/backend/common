package pl.com.rszewczyk.common.api.support.paging;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import lombok.Getter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

@Getter
public class DeserializablePage<T> extends PageImpl<T> {

  protected Pageable pageable;

  @JsonCreator
  public DeserializablePage(
      @JsonProperty("content") List<T> content,
      @JsonProperty("pageable") DeserializablePageRequest pageable,
      @JsonProperty("totalElements") long total) {
    super(content, pageable, total);
    this.pageable = pageable;
  }

  public static <T> DeserializablePage<T> fromPage(Page<T> page) {
    return new DeserializablePage<>(
        page.getContent(), DeserializablePageRequest.fromPage(page), page.getTotalElements());
  }
}
