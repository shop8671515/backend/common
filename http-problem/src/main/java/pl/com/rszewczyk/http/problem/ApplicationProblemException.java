package pl.com.rszewczyk.http.problem;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.http.HttpStatus;
import pl.com.rszewczyk.common.exception.IErrorCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class ApplicationProblemException extends RuntimeException
    implements ProblemMetadataAware, IErrorCode {
  private final String errorCode;
  private final HttpStatus httpStatus;
  private final ProblemLogLevel logLevel;

  public ApplicationProblemException(
      String errorCode, HttpStatus httpStatus, ProblemLogLevel logLevel, Throwable cause) {
    super(errorCode, cause);
    this.errorCode = errorCode;
    this.httpStatus = httpStatus;
    this.logLevel = logLevel;
  }

  public ApplicationProblemException(
      String errorCode,
      HttpStatus httpStatus,
      ProblemLogLevel logLevel,
      String message,
      Throwable cause) {
    super(message, cause);
    this.errorCode = errorCode;
    this.httpStatus = httpStatus;
    this.logLevel = logLevel;
  }

  public ApplicationProblemException(
      String errorCode, HttpStatus httpStatus, ProblemLogLevel logLevel) {
    super(errorCode);
    this.errorCode = errorCode;
    this.httpStatus = httpStatus;
    this.logLevel = logLevel;
  }

  public ApplicationProblemException(
      String errorCode, HttpStatus httpStatus, ProblemLogLevel logLevel, String message) {
    super(message);
    this.errorCode = errorCode;
    this.httpStatus = httpStatus;
    this.logLevel = logLevel;
  }
}
