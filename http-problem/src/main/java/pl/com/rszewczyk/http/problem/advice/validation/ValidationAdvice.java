package pl.com.rszewczyk.http.problem.advice.validation;

import org.springframework.core.annotation.Order;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.NativeWebRequest;

import javax.validation.ConstraintViolationException;
import pl.com.rszewczyk.http.problem.advice.violation.ViolationAdvice;

@Order(100)
@RestControllerAdvice
public class ValidationAdvice extends ViolationAdvice {

    @ExceptionHandler
    public ResponseEntity<Object> handleConstraintViolation(ConstraintViolationException exception, NativeWebRequest request) {
        return createViolationProblem(buildViolations(exception), request);
    }
}
