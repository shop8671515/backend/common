package pl.com.rszewczyk.http.problem;

public interface ProblemProperties {

  boolean isStackTraceEnabled();
}
