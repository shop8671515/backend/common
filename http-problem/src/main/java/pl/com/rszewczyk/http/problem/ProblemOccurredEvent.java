package pl.com.rszewczyk.http.problem;

import org.springframework.context.ApplicationEvent;

public class ProblemOccurredEvent extends ApplicationEvent {

  public ProblemOccurredEvent(Problem source) {
    super(source);
  }

  public Problem getProblem() {
    return (Problem) getSource();
  }
}
