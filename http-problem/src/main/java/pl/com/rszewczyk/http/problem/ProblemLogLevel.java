package pl.com.rszewczyk.http.problem;

import java.util.function.Consumer;
import org.slf4j.Logger;

public enum ProblemLogLevel {
  TRACE {
    @Override
    public Consumer<String> logger(Logger logger) {
      return logger::trace;
    }
  },
  DEBUG {
    @Override
    public Consumer<String> logger(Logger logger) {
      return logger::debug;
    }
  },
  INFO {
    @Override
    public Consumer<String> logger(Logger logger) {
      return logger::info;
    }
  },
  WARN {
    @Override
    public Consumer<String> logger(Logger logger) {
      return logger::warn;
    }
  },
  ERROR {
    @Override
    public Consumer<String> logger(Logger logger) {
      return logger::error;
    }
  };

  public abstract Consumer<String> logger(Logger logger);
}
