package pl.com.rszewczyk.http.problem;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.util.List;
import javax.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Data;
import lombok.Value;
import org.springframework.http.HttpStatus;
import pl.com.bit.http.problem.jackson.StatusTypeDeserializer;
import pl.com.bit.http.problem.jackson.StatusTypeSerializer;

@Data
@Builder
public class Problem {

  @JsonSerialize(using = StatusTypeSerializer.class)
  @JsonDeserialize(using = StatusTypeDeserializer.class)
  @NotNull
  private final HttpStatus status;

  public Problem(
      @JsonProperty("status") HttpStatus status,
      @JsonProperty("detail") String detail,
      @JsonProperty("instance") String instance,
      @JsonProperty("stackTrace") String[] stackTrace,
      @JsonProperty("violations") List<Violation> violations,
      @JsonProperty("errors") List<Error> errors,
      @JsonProperty("logLevel") ProblemLogLevel logLevel) {
    this.status = status;
    this.detail = detail;
    this.instance = instance;
    this.stackTrace = stackTrace;
    this.violations = violations;
    this.errors = errors;
    this.logLevel = logLevel;
  }

  private final String detail;
  private final String instance;
  private final String[] stackTrace;

  private final List<Violation> violations;
  private final List<Error> errors;

  private final ProblemLogLevel logLevel;
}
