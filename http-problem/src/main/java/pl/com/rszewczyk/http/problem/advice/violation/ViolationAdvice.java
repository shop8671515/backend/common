package pl.com.rszewczyk.http.problem.advice.violation;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.NativeWebRequest;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import pl.com.rszewczyk.common.ErrorCode;
import pl.com.rszewczyk.http.problem.Problem;
import pl.com.rszewczyk.http.problem.ProblemLogLevel;
import pl.com.rszewczyk.http.problem.Violation;
import pl.com.rszewczyk.http.problem.advice.Advice;

public class ViolationAdvice extends Advice {

    protected ResponseEntity<Object> createViolationProblem(Collection<Violation> stream, NativeWebRequest request) {
        List<Violation> violations = stream.stream()
                .sorted(Comparator.comparing(Violation::getField).thenComparing(Violation::getMessage))
                .collect(Collectors.toList());

        Problem problem = Problem.builder()
                .status(HttpStatus.UNPROCESSABLE_ENTITY)
                .violations(violations)
                .errors(Collections.singletonList(error(ErrorCode.VIOLATION_EXCEPTION.name())))
                .instance(getInstance(request))
                .logLevel(ProblemLogLevel.TRACE)
                .build();

        return create(problem);
    }

    private Violation createViolation(ConstraintViolation violation) {
        return new Violation(violation.getPropertyPath().toString(), violation.getMessage());
    }

    protected List<Violation> buildViolations(ConstraintViolationException exception) {
        return exception.getConstraintViolations()
                .stream()
                .map(this::createViolation)
                .collect(Collectors.toList());
    }
}
