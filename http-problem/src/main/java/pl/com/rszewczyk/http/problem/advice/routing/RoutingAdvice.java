package pl.com.rszewczyk.http.problem.advice.routing;

import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.NoHandlerFoundException;
import pl.com.rszewczyk.common.ErrorCode;
import pl.com.rszewczyk.http.problem.ProblemLogLevel;
import pl.com.rszewczyk.http.problem.advice.Advice;

@Order(100)
@RestControllerAdvice
public class RoutingAdvice extends Advice {

    @ExceptionHandler
    public ResponseEntity<Object> handleMissingServletRequestParameterException(MissingServletRequestParameterException exception, NativeWebRequest request) {
        return create(HttpStatus.BAD_REQUEST, exception, request, ProblemLogLevel.WARN, error(ErrorCode.BAD_REQUEST.name()));
    }

    @ExceptionHandler
    public ResponseEntity<Object> handleMissingServletRequestPartException(MissingServletRequestPartException exception, NativeWebRequest request) {
        return create(HttpStatus.BAD_REQUEST, exception, request, ProblemLogLevel.WARN, error(ErrorCode.BAD_REQUEST.name()));
    }

    @ExceptionHandler
    public ResponseEntity<Object> handleServletRequestBindingException(ServletRequestBindingException exception, NativeWebRequest request) {
        return create(HttpStatus.BAD_REQUEST, exception, request, ProblemLogLevel.ERROR, error(ErrorCode.BAD_REQUEST.name()));
    }

    @ExceptionHandler
    public ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException exception, NativeWebRequest request) {
        return create(HttpStatus.NOT_FOUND, exception, request, ProblemLogLevel.WARN, error(ErrorCode.BAD_REQUEST.name()));
    }
}
