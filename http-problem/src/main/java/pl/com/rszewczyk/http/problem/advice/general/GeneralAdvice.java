package pl.com.rszewczyk.http.problem.advice.general;

import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.NativeWebRequest;
import pl.com.rszewczyk.common.ErrorCode;
import pl.com.rszewczyk.common.exception.ObjectVersionMismatchException;
import pl.com.rszewczyk.common.exception.ViolationException;
import pl.com.rszewczyk.http.problem.ProblemLogLevel;
import pl.com.rszewczyk.http.problem.advice.Advice;

@Order(200)
@RestControllerAdvice
public class GeneralAdvice extends Advice {

    @ExceptionHandler
    public ResponseEntity<Object> handleThrowable(Throwable exception, NativeWebRequest request) {
        return create(HttpStatus.INTERNAL_SERVER_ERROR, exception, request, ProblemLogLevel.ERROR);
    }

    @ExceptionHandler
    public ResponseEntity<Object> handleThrowable(ViolationException exception, NativeWebRequest request) {
        return create(HttpStatus.UNPROCESSABLE_ENTITY, exception, request, ProblemLogLevel.WARN);
    }

    @ExceptionHandler
    public ResponseEntity<Object> handleUnsupportedOperation(UnsupportedOperationException exception, NativeWebRequest request) {
        return create(HttpStatus.NOT_IMPLEMENTED, exception, request, ProblemLogLevel.WARN, error(ErrorCode.BAD_REQUEST.name()));
    }

    @ExceptionHandler
    public ResponseEntity<Object> handleAccessDeniedException(AccessDeniedException exception, NativeWebRequest request) {
        return create(HttpStatus.FORBIDDEN, exception, request, ProblemLogLevel.INFO, error(ErrorCode.ACCESS_DENIED.name()));
    }

    @ExceptionHandler
    public ResponseEntity<Object> handleElementVersionMismatchException(ObjectVersionMismatchException exception, NativeWebRequest request) {
        return create(HttpStatus.BAD_REQUEST, exception, request, ProblemLogLevel.DEBUG);
    }
}
