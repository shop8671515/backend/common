package pl.com.rszewczyk.http.problem;

public interface ProblemConverter {

  Object convert(Problem problem);
}
