package pl.com.rszewczyk.http.problem;

import org.springframework.http.HttpStatus;

public interface ProblemMetadataAware {

  HttpStatus getHttpStatus();

  ProblemLogLevel getLogLevel();
}
