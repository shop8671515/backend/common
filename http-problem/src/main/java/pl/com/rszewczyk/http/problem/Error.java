package pl.com.rszewczyk.http.problem;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.validation.constraints.NotBlank;
import lombok.Builder;
import lombok.Data;
import lombok.Value;

@Data
@Builder
public class Error {

  @NotBlank private final String code;

  @NotBlank private final String message;

  public Error(@JsonProperty("code") String code, @JsonProperty("message") String message) {
    this.code = code;
    this.message = message;
  }
}
