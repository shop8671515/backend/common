package pl.com.rszewczyk.http.problem.springbootstarter.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;

import static java.util.Optional.ofNullable;
import pl.com.rszewczyk.http.problem.ProblemLogLevel;
import static pl.com.rszewczyk.http.problem.ProblemLogLevel.WARN;
import pl.com.rszewczyk.http.problem.ProblemOccurredEvent;

@Slf4j
public class ProblemLoggingListener {

    @EventListener
    public void handleProblemEvent(ProblemOccurredEvent event) {
        ProblemLogLevel logLevel = ofNullable(event.getProblem().getLogLevel()).orElse(WARN);
        String logMessage = String.format("Problem: {problem: %s}", event.getProblem());
        logLevel.logger(log).accept(logMessage);
    }
}
