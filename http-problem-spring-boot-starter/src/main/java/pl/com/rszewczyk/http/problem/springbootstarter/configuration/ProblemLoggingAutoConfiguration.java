package pl.com.rszewczyk.http.problem.springbootstarter.configuration;

import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import pl.com.rszewczyk.http.problem.springbootstarter.listener.ProblemLoggingListener;

@AutoConfigureBefore(WebMvcAutoConfiguration.class)
@Configuration
@Import(ProblemLoggingListener.class)
public class ProblemLoggingAutoConfiguration {}
