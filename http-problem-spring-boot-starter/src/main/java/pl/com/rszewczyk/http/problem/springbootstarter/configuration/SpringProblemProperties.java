package pl.com.rszewczyk.http.problem.springbootstarter.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "http-problem")
public class SpringProblemProperties {
  private boolean stackTraceEnabled;
}
