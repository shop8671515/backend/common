package pl.com.rszewczyk.common.versioned.entity;


import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.javers.core.metamodel.annotation.DiffIgnore;
import pl.com.rszewczyk.common.named.object.entity.NamedObjectId;
import pl.com.rszewczyk.common.named.object.entity.NamedObjectSnap;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Version;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.OffsetDateTime;
import java.util.UUID;

@Data
@SuperBuilder
@MappedSuperclass
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public abstract class VersionedEntity {

    @Id
    @GeneratedValue
    @EqualsAndHashCode.Include
    @Column(columnDefinition = "uuid", updatable = false)
    private UUID id;

    @NotNull
    private OffsetDateTime created;

    private OffsetDateTime modified;

    @Min(0L)
    @NotNull
    @Version
    private Integer version;

    @Setter(AccessLevel.PRIVATE)
    @NotNull
    @AttributeOverrides({
            @AttributeOverride(name = "id", column = @Column(name = "author_id", updatable = false)),
            @AttributeOverride(name = "type", column = @Column(name = "author_type", updatable = false))
    })
    private NamedObjectId authorId;

    @DiffIgnore
    @ToString.Exclude
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name = "author_id", columnDefinition = "uuid", insertable = false, updatable = false, referencedColumnName = "id"),
            @JoinColumn(name = "author_type", insertable = false, updatable = false, referencedColumnName = "type")
    })
    private NamedObjectSnap author;

    @Setter(AccessLevel.PRIVATE)
    @AttributeOverrides({
            @AttributeOverride(name = "id", column = @Column(name = "modifier_id")),
            @AttributeOverride(name = "type", column = @Column(name = "modifier_type"))
    })
    private NamedObjectId modifierId;

    @DiffIgnore
    @ToString.Exclude
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name = "modifier_id", columnDefinition = "uuid", insertable = false, updatable = false, referencedColumnName = "id"),
            @JoinColumn(name = "modifier_type", insertable = false, updatable = false, referencedColumnName = "type")
    })
    private NamedObjectSnap modifier;

    public void setAuthor(@NotNull NamedObjectSnap author) {
        this.author = author;
        this.authorId = author.getCid();
    }

    public void setModifier(@NotNull NamedObjectSnap modifier) {
        this.modifier = modifier;
        this.modifierId = modifier.getCid();
    }

    @PrePersist
    private void onCreate() {
        created = OffsetDateTime.now();
    }

    @PreUpdate
    private void onUpdate() {
        modified = OffsetDateTime.now();
    }

    public static abstract class VersionedEntityBuilder<C extends VersionedEntity, B extends VersionedEntityBuilder<C, B>> {
        public B copyVersionedDataFrom(VersionedEntity versionedEntity) {
            id(versionedEntity.id);
            created(versionedEntity.created);
            modified(versionedEntity.modified);
            version(versionedEntity.version);
            authorId(versionedEntity.authorId);
            author(versionedEntity.author);
            modifierId(versionedEntity.modifierId);
            modifier(versionedEntity.modifier);
            return self();
        }
    }
}
